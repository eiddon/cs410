module Ex6-4-Dis where

open import Ex6-Setup
open import Ex6-1-Vec
open import Ex6-2-Box
open import Ex6-3-Cut


---------------------------------------------------------------------------
-- CURSES DISPLAY FOR APPLICATIONS (5 marks)                             --
---------------------------------------------------------------------------

-- You may need to look at the Ex6-Setup file to find some of the relevant
-- kit for this episode, and it's worth looking there for goodies, anyway.
-- We start from the idea of a main loop.

{- This is the bit of code I wrote in Haskell to animate your code. -}
postulate
  mainLoop : {S : Set} ->             -- program state
    -- INITIALIZER
    S ->                              -- initial state
    -- EVENT HANDLER
    (Event -> S ->                    -- event and state in
     S /**/ List Action) ->           -- new state and screen actions out
    -- PUT 'EM TOGETHER AND YOU'VE GOT AN APPLICATION!
    IO Thud
{-# COMPILED mainLoop (\ _ -> HaskellSetup.mainLoop) #-}

-- The type S /**/ T is a type of pairs that the compiler can share with
-- Haskell. Its constructor is _,_ just as for S /*/ T. Meanwhile Thud
-- does the same job for One, but you won't need to bother with it.

-- To make a thing you can run, you need to
--   (i)    choose a type to represent the program's internal state S
--   (ii)   give the initial state
--   (iii)  explain how, given an event and the current state, to
--            produce a new state and a list of actions to update the
--            display.

-- Let me show you the example I gave in class...


---------------------------------------------------------------------------
-- My silly wee demo                                                     --
---------------------------------------------------------------------------

-- the program state...

SillyState : Set
SillyState = Char /*/ Nat /*/ Nat

-- ...knows the character to put round the edge of the window, and the
-- size of the window

-- To paint a window whose dimensions are both at least 2, we...

sillyPaint : SillyState -> List Action
sillyPaint (c , suc (suc w), suc (suc h))
  =   goRowCol 0 0                   -- send the cursor home
  ::  bgText black :: fgText green   -- adopt our colour scheme
  ::  sendText (concat (             -- send ...   
        list (suc (suc w)) c ::                           -- ... top
        concat (list h (c :: list w ' ' +-+ c :: [])) ::  -- middle
        list (suc (suc w)) c :: [])) ::                   -- and bottom
        []
sillyPaint _ = []

-- There are two kinds of event we must consider: keystroke and resize.

sillyHandler : Event -> SillyState -> SillyState /**/ List Action
sillyHandler (key (char c)) (_ , w , h) = s , sillyPaint s where
  -- if we get an ordinary character key,
  -- update the character and repaint
  s : SillyState
  s = (c , w , h)
sillyHandler (resize w h) (c , _ , _) = s , sillyPaint s where
  -- if we get a resizing event,
  -- update the size and repaint
  s : SillyState
  s = (c , w , h)
sillyHandler _ s = s , []   -- otherwise, relax

-- To finish the job, we write a "main" function, plugging in the initial
-- state. The initial size of 0 by 0 will provoke an immediate resize event,
-- giving the correct size!

{-
main : IO Thud
main = mainLoop ('*' , 0 , 0) sillyHandler
 -}

-- To run this program, start a terminal, change to your Ex6 directory, then
--
--    make go4
--
-- You should be able to press keys and resize the thing and see sensible
-- stuff happen. Ctrl-C quits.

-- When you're bored of green rectangles, comment out the above main
-- function, so you can move on to the actual work. There are other
-- main functions further down the file which you can comment in as you
-- need them. Of course, you can have at most one main at a time.


---------------------------------------------------------------------------
-- PAINTINGS                                                             --
---------------------------------------------------------------------------

-- Now that we're in colour, one cell of display will be a ColourChar ...

data ColourChar : Set where
  _-_/_ : (fg : Colour)(c : Char)(bg : Colour) -> ColourChar

-- ... e.g.     green - '*' / black    for what we had, above.

-- a painting is a Box structure whose basic tiles are either transparent
-- holes or opaque rectangles of coloured text.

Painting : Nat -> Nat -> Set
Painting = Box (HoleOr (Matrix ColourChar))

-- Now your turn. Making use of the equipment you developed in epsiode 2,
-- get us from a Painting to a List Action in two hops. Note that you will
-- have to decide how to render a Hole: some bland background stuff, please.
-- (1 mark)

space : ColourChar
space = black - ' ' / white

cHoleSpace : HoleOr (Matrix ColourChar) []> Matrix ColourChar
cHoleSpace Hole = vec (vec space)
cHoleSpace [ x ] = x

paintMatrix : Painting []> Matrix ColourChar
paintMatrix = pasteBox matrixPasteKit o mapBox cHoleSpace

paintRow : {w : Nat} -> Vec ColourChar w -> List Action
paintRow [] = []
paintRow (fg - c / bg :: cs) = bgText bg :: fgText fg :: sendText (c :: []) :: paintRow cs

paintAction : {w h : Nat} -> Matrix ColourChar w h -> List Action
paintAction [] = []
paintAction {w} (r :: rs) = paintRow r +-+ move left w :: move down 1 :: paintAction rs


---------------------------------------------------------------------------
-- APPLICATIONS                                                          --
---------------------------------------------------------------------------

-- Here's a general idea of what it means to be an "application".
-- You need to choose some sort of size-dependent state, then provide these
-- bits and pieces. We need to know how the state is updated according to
-- events, with resizing potentially affecting the state's type. We must
-- be able to paint the state. The state should propose a cursor position.
-- (Keen students may modify this definition to ensure the cursor must be
-- within the bounds of the application.)

record Application (S : Nat -> Nat -> Set) : Set where
  field
    handleKey     : Key -> S []> S
    handleResize  : {w h : Nat}(w' h' : Nat) -> S w h -> S w' h'
    paintMe       : S []> Painting
    cursorMe      : {w h : Nat} -> S w h -> Nat /*/ Nat  -- x,y coords
open Application

-- Now your turn. Build the appropriate handler to connect these
-- applications with mainLoop. Again, work in two stages, first
-- figuring out how to do the right actions, then managing the
-- state properly. (1 mark)

AppState : (S : Nat -> Nat -> Set) -> Set
AppState S = Sg Nat \ w -> Sg Nat \ h -> S w h

appPaint : {S : Nat -> Nat -> Set}{w h : Nat} ->
           Application S -> S w h -> List Action
appPaint app s = goRowCol 0 0 :: paintAction (paintMatrix p) +-+ goRowCol (fst xy) (snd xy) :: []
  where
    p  = paintMe app s        -- a wee reminder of how to use record stuff
    xy = cursorMe app s

appHandler : {S : Nat -> Nat -> Set} ->
           Application S ->
           Event -> AppState S -> AppState S /**/ List Action
appHandler {S} app (key k) (w , h , s') = (w , h , s) , appPaint app s where
  s : S w h
  s = handleKey app k s'
appHandler {S} app (resize w h) (_ , _ , s') = (w , h , s) , appPaint app s where
  s : S w h
  s = handleResize app w h s'
-- Your code turns into a main function, as follows.
-- s' is the wrong thing to apply to appPaint but it's the only thing
-- I can do with app and app is the only Application S I have

appMain : {S : Nat -> Nat -> Set} -> Application S -> S 0 0 -> IO Thud
appMain app s = mainLoop (0 , 0 , s) (appHandler app) 


---------------------------------------------------------------------------
-- THE DEMO, MADE INTO AN APPLICATION                                    --
---------------------------------------------------------------------------

sillyChar : Char -> {w h : Nat} -> Painting w h
sillyChar c = [ [ vec (vec (green - c / black)) ] ]

sillyApp : Application \ _ _ -> Char
sillyApp = record
  {  handleKey     = \ { (char c) _ -> c ; _ c -> c }
  ;  handleResize  = \ _ _ c -> c
  ;  paintMe       = \
       { {suc (suc w)} {suc (suc h)} c ->
          tobo 1 (sillyChar c)
          (suc h) (tobo h
            (leri 1 (sillyChar c) (suc w)
             (leri w (sillyChar ' ') 1 (sillyChar c) (plusCommFact 1 w))
             refl)
            1 (sillyChar c) (plusCommFact 1 h) )
          refl
       ; c -> sillyChar c
       }
  ;  cursorMe      = \ _ -> 0 , 0
  }

 
{-
main : IO Thud
main = appMain sillyApp '*'
 -}


---------------------------------------------------------------------------
-- COMPARING TWO NUMBERS                                                 --
---------------------------------------------------------------------------

-- You've done the tricky part in episode 3, comparing two splittings of
-- the same number. Here's an easy way to reuse that code just to compare
-- two numbers. It may help in what is to come.

Compare : Nat -> Nat -> Set
Compare x y = CutCompare x y y x (x + y)

compare : (x y : Nat) -> Compare x y
compare x y = cutCompare x y y x (x + y) refl (plusCommFact x y)

-- To make sure you've got the message, try writing these things
-- "with compare" to resize paintings. If you need to make a painting
-- bigger, pad its right or bottom with a hole. If you need to make it
-- smaller, trim off the right or bottom excess. You have all the gadgets
-- you need! I'm not giving marks for these, but they'll be useful in
-- the next bit.

paintingCutKit : {X : Set} -> CutKit (Box (HoleOr (Matrix X)))
paintingCutKit = boxCutKit (holeCutKit matrixCutKit)


cropPadLR : (w h w' : Nat) -> Painting w h -> Painting w' h
cropPadLR w h w' p with compare w w'
cropPadLR w h .w p | onJoin refl refl = p
cropPadLR w h .(w + r) p | left r refl p' = leri w p r [ Hole ] refl
cropPadLR .(w' + r) h w' p | right r refl p' = Sg.fst (CutKit.cutLR paintingCutKit (w' + r) h w' r refl p)

cropPadTB : (w h h' : Nat) -> Painting w h -> Painting w h'
cropPadTB w h h' p with compare h h'
cropPadTB w h .h p | onJoin refl refl = p
cropPadTB w h .(h + r) p | left r refl p' = tobo h p r [ Hole ] refl
cropPadTB w .(h' + r) h' p | right r refl p' = Sg.fst (CutKit.cutTB paintingCutKit w (h' + r) h' r refl p)

cropPad : {w h : Nat}{w' h' : Nat} -> Painting w h -> Painting w' h'
cropPad {w} {h} {w'} {h'} p = cropPadTB w' h h' (cropPadLR w h w' p)

---------------------------------------------------------------------------
-- THE MOVING RECTANGLE                                                  --
---------------------------------------------------------------------------

-- This is the crux of the episode. Please build me an application which
-- displays a movable resizeable rectangle drawn with ascii art as follows
--
--           +--------------+
--           |              |
--           |              |
--           +--------------+
--
-- The "size" of the application is the terminal size: the rectangle should
-- be freely resizable *within* the terminal, so you should pad out the
-- rectangle to the size of the screen using Hole.
-- That is, only the rectangle is opaque; the rest is transparent.
-- The background colour of the rectangle should be given as an argument.
-- The foreground colour of the rectangle should be white.
-- The rectangle should have an interior consisting of opaque space with
-- the given background colour.
--
-- The arrow keys should move the rectangle around inside the terminal
-- window, preserving its size (like when you drag a window around).
-- Shifted arrow keys should resize the rectangle by moving its bottom
-- right corner (again, like you might do with a mouse).
-- You do not need to ensure that the rectangle fits inside the terminal.
-- The cursor should sit at the bottom right corner of the rectangle.
--
-- Mac users: the Terminal application which ships with OS X does NOT
-- give the correct treatment to shift-up and shift-down. You can get a
-- suitable alternative from http://iterm2.com/ (Thank @sigfpe for the tip!)
--
-- (2 marks, one for key handling, one for painting)

data Rect : Set where
  rect : (rw rh rm dm : Nat) -> Rect

rHandleKey : Key -> (\w h -> Rect) []> (\w h -> Rect)
rHandleKey (arrow normal up) (rect rw rh rm zero) = rect rw rh rm zero
rHandleKey (arrow normal up) (rect rw rh rm (suc dm)) = rect rw rh rm dm
rHandleKey (arrow normal down) (rect rw rh rm dm) = rect rw rh rm (suc dm)
rHandleKey (arrow normal left) (rect rw rh zero dm) = rect rw rh zero dm
rHandleKey (arrow normal left) (rect rw rh (suc rm) dm) = rect rw rh rm dm
rHandleKey (arrow normal right) (rect rw rh rm dm) = rect rw rh (suc rm) dm
rHandleKey (arrow shift up) (rect rw zero rm dm) = rect rw zero rm dm
rHandleKey (arrow shift up) (rect rw (suc rh) rm dm) = rect rw rh rm dm
rHandleKey (arrow shift down) (rect rw rh rm dm) = rect rw (suc rh) rm dm
rHandleKey (arrow shift left) (rect zero rh rm dm) = rect zero rh rm dm
rHandleKey (arrow shift left) (rect (suc rw) rh rm dm) = rect rw rh rm dm
rHandleKey (arrow shift right) (rect rw rh rm dm) = rect (suc rw) rh rm dm
rHandleKey _ whs = whs

rCursorMe : Rect -> Nat /*/ Nat
rCursorMe (rect rw rh rm dm) = suc rh + dm , suc rw + rm

wrap : {w h : Nat} -> ColourChar -> Char -> Painting (suc (suc w)) h
wrap {w}{h = zero} (fg - cc / bg) c = [ Hole ]
wrap {w}{h = suc h} (fg - cc / bg) c = tobo 1 (leri 1 [ [ vec (vec (fg - cc / bg)) ] ] (suc w) (leri w [ [ vec (vec (fg - c / bg)) ] ] 1 [ [ vec (vec (fg - cc / bg)) ] ] (addOneFact w)) refl) h (wrap (fg - cc / bg) c) refl

rectPaint : {w h : Nat} -> Colour -> Rect -> Painting w h
rectPaint {w} {h} c (rect rw rh rm dm) = cropPadLR (rm + suc (suc rw)) h w (cropPadTB (rm + suc (suc rw)) (dm + suc (suc rh)) h
  (tobo
  dm [ Hole ]
  (suc (suc rh)) (leri rm [ Hole ] (suc (suc rw)) pRect refl) refl)) where
  pRect = (tobo 1 (wrap (white - '+' / c) '-') (suc rh) (tobo rh (wrap (white - '|' / c) ' ') 1 (wrap (white - '+' / c) '-') (addOneFact rh)) refl)

rPaintMe : Colour -> (\w h -> Rect) []> Painting
rPaintMe c (rect rw rh rm dm) = rectPaint c (rect rw rh rm dm)


rectApp : Colour -> Application \ w h -> Rect
rectApp c = record
  {  handleKey     = rHandleKey
  ;  handleResize  = \ w' h' x → x
  ;  paintMe       = rPaintMe c
  ;  cursorMe      = rCursorMe
  }

{- -
main : IO Thud
main = appMain (rectApp blue) (rect 10 20 0 0)
 -}

---------------------------------------------------------------------------
-- TWO BECOME ONE                                                        --
---------------------------------------------------------------------------

-- Write a function which turns two sub-applications into one main
-- application by layering them.
--
-- For some S and T, you get an Application S and an Application T
-- You should choose a suitable state representation so that you know
--   (i)   which of the two applications is at the front, and which behind
--   (ii)  the states of both.
--
-- The Tab key should swap which sub-application is at the front, as if you had
-- clicked on the one at the back. All other keys should be handled by
-- whichever action is in front at the time. Also, the cursor position
-- should be chosen by the sub-application at the front.
--
-- The overall application size will be used as the size for both
-- sub-application sizes, which means you should be able to compute the
-- layered Painting, using equipment from episode 3. Crucially, we should be
-- able to see through the holes in the front sub-application to stuff from
-- the back sub-application.
--
-- (1 mark)

data FrontBack (S T : Nat -> Nat -> Set)(w h : Nat) : Set where
  fb : S w h -> T w h -> FrontBack S T w h
  bf : T w h -> S w h -> FrontBack S T w h

fbHandleKey : {S T : Nat -> Nat -> Set} -> Application S -> Application T -> Key -> FrontBack S T []> FrontBack S T
fbHandleKey appS appT tab (fb s t) = bf t s
fbHandleKey appS appT tab (bf t s) = fb s t
fbHandleKey appS appT k (fb s t) = fb (handleKey appS k s) t
fbHandleKey appS appT k (bf t s) = bf (handleKey appT k t) s

fbHandleResize : {S T : Nat -> Nat -> Set} -> Application S -> Application T -> {w h : Nat}(w' h' : Nat) -> FrontBack S T w h -> FrontBack S T w' h'
fbHandleResize appS appT w' h' (fb s t) = fb (handleResize appS w' h' s) (handleResize appT w' h' t)
fbHandleResize appS appT w' h' (bf t s) = bf (handleResize appT w' h' t) (handleResize appS w' h' s)

fbPaintMe : {S T : Nat -> Nat -> Set} -> Application S -> Application T -> FrontBack S T []> Painting
fbPaintMe appS appT (fb s t) = overlay matrixCutKit (paintMe appS s) (paintMe appT t)
fbPaintMe appS appT (bf t s) = overlay matrixCutKit (paintMe appT t) (paintMe appS s)

fbCursorMe : {S T : Nat -> Nat -> Set} -> Application S -> Application T -> {w h : Nat} -> FrontBack S T w h -> Nat /*/ Nat
fbCursorMe appS appT (fb s _) = cursorMe appS s
fbCursorMe appS appT (bf t _) = cursorMe appT t

frontBack : {S T : Nat -> Nat -> Set} ->
  Application S ->
  Application T ->
  Application \ w h -> FrontBack S T w h
frontBack appS appT = record
  { handleKey = fbHandleKey appS appT
  ; handleResize = fbHandleResize appS appT
  ; paintMe = fbPaintMe appS appT
  ; cursorMe = fbCursorMe appS appT
  }
-- By way of example, let's have a blue rectangle and a red rectangle.

{-
main : IO Thud
main = appMain (frontBack (rectApp blue) (rectApp red)) (fb (rect 10 40 3 15) (rect 20 40 8 15))
-}
---------------------------------------------------------------------------
-- NEXT TIME ...                                                         --
---------------------------------------------------------------------------

-- You get to figure out how to reduce flicker.
-- You get to think up some fun stuff to put in the rectangles.


