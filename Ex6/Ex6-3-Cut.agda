module Ex6-3-Cut where

open import Ex6-Setup
open import Ex6-1-Vec
open import Ex6-2-Box

---------------------------------------------------------------------------
-- CUTTING UP BOXES (5 marks)                                            --
---------------------------------------------------------------------------

-- Previously...
-- ... we established what it is to be a CutKit, and we built CutKits
-- for some sorts of basic tile. Now we need to build the CutKit for
-- Box. Let's think about what that involves for a moment. We're going
-- to need a CutKit for basic tiles to stand a chance. But how do we
-- cut compound tiles?
--
-- Suppose we're writing cutLR, and we have some
--   cq : cwl + cwr == w   -- the "cut widths" equation
-- telling us where we want to make the cut in something of width w.
--
--             v
--    +--------:------+
--    |        :      |
--    |        :      |
--    +--cwl---:-cwr--+
--    :        ^      :
--    :.......w.......:
--
-- The tricky part is when the box we're cutting here is built with
--   leri bwl bl bwr br bq
-- where
--   bq : bwl + bwr == w   -- the "box widths" equation
--
-- There are three possible situations, all of which you must detect
-- and handle.
--
-- (i) you hit the sweet spot...
--
--             v
--    +--bwl---+-bwr--+
--    |        |      |
--    |        |      |
--    +--cwl---+-cwr--+
--    :        ^      :
--    :.......w.......:
--
--     ...where the box is already divided in the place where the cut
--     has to go. Happy days.
--
-- (ii) you're cutting to the left of the join...
--
--             v
--    +--bwl-----+bwr-+
--    |        : |    |
--    |        : |    |
--    +--cwl---:-cwr--+
--    :        ^      :
--    :.......w.......:
--
--     ...so you'll need to cut the left box in the correct place. You
--     will need some evidence about widths to do that. And then you'll
--     the have three pieces you can see in my diagram, so to complete
--     the cut, you will need to put two of those pieces together, which
--     will take more evidence.
--
-- (iii) you're cutting to the right of the join...
--
--             v
--    +--bwl-+--bwr---+
--    |      | :      |
--    |      | :      |
--    +--cwl---:-cwr--+
--    :        ^      :
--    :.......w.......:
--
--     ...so you'll need to cut the right box in the correct place, and
--     reassemble the bits.
--
-- HINT: THE FIRST THREE MARKS IN THIS EPISODE COME FROM ONE PROBLEM.
-- TREAT THEM AS A WHOLE.


---------------------------------------------------------------------------
-- COMPARING THE CUT POSITION                                            --
---------------------------------------------------------------------------

data CutCompare (x x' y y' n : Nat) : Set where
  onJoin : (l : x == y)(r : x' == y') -> CutCompare x x' y y' n
  left   : (r : Nat)(p : x + r == y)(p' : r + y' == x') -> CutCompare x x' y y' n
  right  : (r : Nat)(p : y + r == x)(p' : r + x' == y') -> CutCompare x x' y y' n
  -- Give three constructors for this type which characterize the three
  -- possibilities described above whenever
  --   x + x' == n   and   y + y' == n
  -- (E.g., take n to be w, x and x' to be cwl and cwr, y and y' to be
  -- bwl and bwr. But later, you'll need to do use the same tool for
  -- heights.)
  --
  -- You will need to investigate what evidence must be packaged in each
  -- situation. On the one hand, you need to be able to *generate* the
  -- evidence, with cutCompare, below. On the other hand, the evidence
  -- must be *useful* when you come to write boxCutKit, further below.
  -- Don't expect to know what to put here from the get-go. Figure it
  -- out by discovering what you *need*.
  --
  -- (1 mark)

-- Show that whenever you have two ways to express the same n as a sum,
-- you can always deliver the CutCompare evidence. (1 mark)

cutCompare : (x x' y y' n : Nat) -> x + x' == n -> y + y' == n ->
             CutCompare x x' y y' n
cutCompare zero x' zero .x' .x' refl refl = onJoin refl refl
cutCompare zero .(suc (y + y')) (suc y) y' .(suc (y + y')) refl refl = left (suc y) refl refl
cutCompare (suc x) x' zero .(suc (x + x')) .(suc (x + x')) refl refl = right (suc x) refl refl
cutCompare (suc x) x' (suc y) y' zero () yq
cutCompare (suc x) x' (suc y) y' (suc .(x + x')) refl yq with cutCompare x x' y y' (x + x') refl (sucInj yq)
cutCompare (suc x) x' (suc y) y' (suc .(x + x')) refl yq | onJoin l r = onJoin (sucResp l) r
cutCompare (suc x) x' (suc y) y' (suc .(x + x')) refl yq | left r p p' = left r (sucResp p) p'
cutCompare (suc x) x' (suc y) y' (suc .(x + x')) refl yq | right r p p' = right r (sucResp p) p'

---------------------------------------------------------------------------
-- A CUTKIT FOR BOXES                                                    --
---------------------------------------------------------------------------

-- Now, show that you can construct a CutKit for Box X, given a CutKit
-- for X. There will be key points where you get stuck for want of crucial
-- information. The purpose of CutCompare is to *describe* that
-- information. The purpose of cutCompare is to *compute* that information.
-- Note that cutLR and cutTB will work out very similarly, just exchanging
-- the roles of width and height.
-- (1 mark)

boxCutKit : {X : Nat -> Nat -> Set} -> CutKit X -> CutKit (Box X)
boxCutKit {X} ck = record { cutLR = clr ; cutTB = ctb } where
  open CutKit ck
  clr : (w h wl wr : Nat) ->
          wl + wr == w -> Box X w h -> Box X wl h /*/ Box X wr h
  clr w h cwl cwr wq [ x ] with cutLR w h cwl cwr wq x
  clr w h cwl cwr wq [ x ] | l , r = [ l ] , [ r ]
  clr w h cwl cwr wq (leri wl l wr r x) with cutCompare cwl cwr wl wr w wq x
  clr w h cwl cwr wq (leri .cwl l' .cwr r' x) | onJoin refl refl = l' , r'
  clr .(wl + wr) h cwl cwr wq (leri wl l wr r refl) | left r' p p' with clr wl h cwl r' p l
  clr .(wl + wr) h cwl cwr wq (leri wl l wr r refl) | left r' p p' | fst , snd = fst , leri r' snd wr r p'
  clr w h cwl cwr wq (leri wl l wr r x) | right r' p p' with clr wr h r' cwr p' r
  clr w h cwl cwr wq (leri wl l wr r x) | right r' p p' | fst , snd = leri wl l r' fst p , snd
  clr w h cwl cwr hq (tobo ht t hb b x) with (clr w ht cwl cwr hq t)
  clr w h cwl cwr hq (tobo ht t hb b x) | tl , tr with clr w hb cwl cwr hq b
  clr w h cwl cwr hq (tobo ht t hb b x) | tl , tr | bl , br = tobo ht tl hb bl x , tobo ht tr hb br x
  ctb : (w h ht hb : Nat) ->
          ht + hb == h -> Box X w h -> Box X w ht /*/ Box X w hb
  ctb w h ht hb hq [ x ] with cutTB w h ht hb hq x
  ctb w h cht chb hq [ x ] | t , b = [ t ] , [ b ]
  ctb w h cht chb hq (tobo ht t hb b x) with cutCompare cht chb ht hb h hq x
  ctb w h cht chb hq (tobo .cht t .chb b x) | onJoin refl refl = t , b
  ctb w h cht chb hq (tobo ht t hb b x) | left r p p' with ctb w ht cht r p t
  ctb w h cht chb hq (tobo ht t hb b x) | left r p p' | t' , b' = t' , tobo r b' hb b p'
  ctb w h cht chb hq (tobo ht t hb b x) | right r p p' with ctb w hb r chb p' b
  ctb w h cht chb hq (tobo ht t hb b x) | right r p p' | t' , b' = tobo ht t r t' p , b'
  ctb w h cht chb hq (leri wl l wr r x) with (ctb wl h cht chb hq l)
  ctb w h cht chb hq (leri wl l wr r x) | tl , bl with ctb wr h cht chb hq r
  ctb w h cht chb hq (leri wl l wr r x) | tl , bl | tr , br = leri wl tl wr tr x , leri wl bl wr br x



---------------------------------------------------------------------------
-- CROP                                                                  --
---------------------------------------------------------------------------

-- Show that, given a CutKit, you can implement the "crop" operation which
-- trims a small rectangle out of an enclosing rectangle.
-- (1 mark)

crop : {X : Nat -> Nat -> Set} -> CutKit X ->
       (wl wc wr ht hc hb : Nat) ->
       X (wl + wc + wr) (ht + hc + hb) -> X wc hc
crop ck wl wc wr ht hc hb x with Sg.snd (CutKit.cutLR ck (wl + wc + wr) (ht + hc + hb) wl (wc + wr) refl x)
crop ck wl wc wr ht hc hb x | wcwr with Sg.fst (CutKit.cutLR ck (wc + wr) (ht + hc + hb) wc wr refl wcwr)
crop ck wl wc wr ht hc hb x | wcwr | wc' with Sg.snd (CutKit.cutTB ck wc (ht + hc + hb) ht (hc + hb) refl wc')
crop ck wl wc wr ht hc hb x | wcwr | wc' | hchb = Sg.fst (CutKit.cutTB ck wc (hc + hb) hc hb refl hchb)

-- For fun, practice, and the chance to test your work, try building
-- a nontrivially tiled...

addOneFact : (x : Nat) -> x + suc zero == suc x
addOneFact zero = refl
addOneFact (suc x) = sucResp (addOneFact x)

boringQuilt :{w h : Nat} -> Char -> _*C*_ w h
boringQuilt c = vec (vec c)

edgedQuilt : (w h : Nat) -> Char -> Box (HoleOr _*C*_) (suc (suc w)) (suc (suc h))
edgedQuilt w h c = tobo (suc zero) [ [ vec (vec c) ] ] (suc h) (tobo h (leri (suc zero) [ [ vec (vec c) ] ] (suc w) (leri w [ Hole ] (suc zero) [ [ vec (vec c) ] ] (addOneFact w)) refl) (suc zero) [ [ vec (vec c) ] ] (addOneFact h)) refl

--{![ leri ? [ [ vec (vec c) ] ] (suc w) (leri w [ Hole ] {!!} [ [ vec (vec c) ] ] {!!}) {!!} ]!})
testBigBox : Box (HoleOr _*C*_) 20 15
testBigBox = tobo 8 (leri 10 (edgedQuilt (suc (suc (suc (suc (suc (suc (suc (suc zero)))))))) (suc (suc (suc (suc (suc (suc zero)))))) 'x') 10 (edgedQuilt (suc (suc (suc (suc (suc (suc (suc (suc zero)))))))) (suc (suc (suc (suc (suc (suc zero)))))) 'o') refl) 7 (leri 10 (edgedQuilt (suc (suc (suc (suc (suc (suc (suc (suc zero)))))))) (suc (suc (suc (suc (suc zero))))) '#') 10 (edgedQuilt (suc (suc (suc (suc (suc (suc (suc (suc zero)))))))) (suc (suc (suc (suc (suc zero))))) '*') refl) refl

-- ...so that you can see this stuff in action:

textDisplayCutKit : CutKit (Box (HoleOr _*C*_))
textDisplayCutKit = boxCutKit (holeCutKit matrixCutKit)

testWeeBox : Box (HoleOr _*C*_) 10 5
testWeeBox = crop textDisplayCutKit 5 10 5 5 5 5 testBigBox

betterTest : Box (HoleOr _*C*_) _ _
betterTest = crop textDisplayCutKit 6 8 6 4 8 3 testBigBox


---------------------------------------------------------------------------
-- OVERLAY                                                               --
---------------------------------------------------------------------------

-- If we use HoleOr X as the basic tile, we can think of Hole as meaning
-- a bit of a box we can see through. Correspondingly, if we have two
-- boxes (the "front" one and the "back" one), both the same size, we
-- should be able to see through the holes in the front box to whatever
-- stuff is in the back box in the corresponding place.
--
-- Your task here is to show that you can combine front and back layers
-- into a single box, corresponding to what you would actually see. That
-- is, you will need to fill the front holes in with stuff cut from the
-- back. Which is why you need a CutKit for boxes.
--
-- Hint: you may be tempted to use crop, but try without crop first.
--
-- (1 mark)

overlay : {X : Nat -> Nat -> Set} -> CutKit X ->
          {w h : Nat} ->
          {- front -}     Box (HoleOr X) w h ->
          {- back  -}     Box (HoleOr X) w h ->
          {- combined -}  Box (HoleOr X) w h
overlay {X} ck = go where
  open CutKit (boxCutKit (holeCutKit ck))
  go : {w h : Nat} ->
       Box (HoleOr X) w h -> Box (HoleOr X) w h -> Box (HoleOr X) w h
  go [ Hole ] back = back
  go [ [ x ] ] back = [ [ x ] ]
  go {w} {h} (leri wl l wr r x) back with cutLR w h wl wr x back
  go (leri wl l wr r x) back | backl , backr = leri wl (go l backl) wr (go r backr) x
  go {w} {h} (tobo ht t hb b x) back with cutTB w h ht hb x back -- I didn't stricly need to bring h into scope but there's no harm. 
  go (tobo ht t hb b x) back | backt , backb = tobo ht (go t backt) hb (go b backb) x

-- You should ensure (but I won't ask you to prove) that you have thus
-- equipped Box (HoleOr X) w h with the structure of a *monoid* with
-- the neutral value (nil-like thing) being [ Hole ] and the
-- associative operation (append-like thing) being (overlay ck), where
-- ck is your CutKit X. That is, there is such a thing as a totally
-- transparent layer, and you can overlay *any* number of layers by
-- combining any two neighbouring layers at a time.

-- For fun, and the shape of things to come, build two box tilings.
-- Make sure each has a rectangle of text in the middle and Hole all
-- around. Make sure that the rectangles overlap each other, but not
-- completely. See what happens when you overlay them, either way
-- around.

padWithHoles : {X : Nat -> Nat -> Set} -> (wl wp wr ht hp hb : Nat) -> X wp hp -> Box (HoleOr X) (wl + wp + wr) (ht + hp + hb)
padWithHoles wl wp wr ht hp hb x = tobo ht [ Hole ]
                                        (hp + hb) (tobo hp (leri wl [ Hole ] (wp + wr) (leri wp  [ [ x ] ] wr [ Hole ] refl) refl)
                                                   hb [ Hole ]
                                                   refl)
                                   refl

holeJoin : forall {X} -> HoleOr (HoleOr X) []> HoleOr X
holeJoin Hole = Hole
holeJoin [ Hole ] = Hole
holeJoin [ [ x ] ] = [ x ]

rectangleA : Box (HoleOr _*C*_) 20 15
rectangleA = padWithHoles 5 10 5 2 10 3 (boringQuilt '#')

rectangleB : Box (HoleOr _*C*_) 20 15
rectangleB = padWithHoles 6 10 4 3 10 2 (boringQuilt '$')

rectangleC : Box (HoleOr _*C*_) 20 15
rectangleC = edgedQuilt 18 13 '*'

rectangleD : Box (HoleOr _*C*_) 20 15
rectangleD = [ [ boringQuilt '&' ] ] 

ov : {w h : Nat} -> (f b : Box (HoleOr _*C*_) w h) -> Box (HoleOr _*C*_) w h
ov = overlay matrixCutKit

frontA_backBO : Box (HoleOr _*C*_) _ _
frontA_backBO = ov rectangleA rectangleB

frontB_backAO : Box (HoleOr _*C*_) _ _
frontB_backAO = ov rectangleB rectangleA

frontC_backDO : Box (HoleOr _*C*_) 20 15
frontC_backDO = ov rectangleC rectangleD

frontTestBackD : Box (HoleOr _*C*_) 8 8
frontTestBackD = crop textDisplayCutKit 6 8 6 4 8 3 (ov testBigBox rectangleD)

frontA_backB : 20 *C* 15
frontA_backB = renderHoleOrText frontA_backBO

frontB_backA : 20 *C* 15
frontB_backA = renderHoleOrText frontB_backAO
