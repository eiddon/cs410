module Ex3 where -- 14.5/15

open import Ex1Prelude
open import FuncKit

{- 3.1 Numbers in the Kit -}

{- We can define the type of natural numbers using the tools
   from the functor kit like this: -}

kNat : Kit
kNat = kK One k+ kId

NAT : Set
NAT = Data kNat

-- Define the function which sends "ordinary" numbers to
-- the corresponding kit-encoded numbers.

Nat2NAT : Nat -> NAT
Nat2NAT zero = [ inl <> ]
Nat2NAT (suc n) = [ inr (Nat2NAT n) ]

-- Use fold to define the function which sends them back.

NatVal : (One /+/ Nat) -> Nat
NatVal (inl x) = 0
NatVal (inr x) = x

NAT2Nat : NAT -> Nat
NAT2Nat = fold (kK One k+ kId) (\ { (inl _) -> 0
                                  ; (inr x) -> suc x
                                  })

-- Show that you get the "round trip" property (by writing
-- recursive functions that use rewrite.

Nat2NAT2Nat : NAT2Nat o Nat2NAT =^= id
Nat2NAT2Nat zero = refl
Nat2NAT2Nat (suc n) rewrite Nat2NAT2Nat n = refl

NAT2Nat2NAT : Nat2NAT o NAT2Nat =^= id
NAT2Nat2NAT [ inl x ] = refl
NAT2Nat2NAT [ inr x ] rewrite NAT2Nat2NAT x = refl


{- 3.2 Lists in the Kit -}

-- find the code which gives you lists with a given element
-- type (note that the kId constructor marks the place for
-- recursive *sublists* not for list elements

kLIST : Set -> Kit
kLIST A = kK One k+ (kK A k* kId)

LIST : Set -> Set
LIST A = Data (kLIST A)

-- define nil and cons for your lists

nil : {A : Set} -> LIST A
nil =  [ inl <> ]

cons : {A : Set} -> A -> LIST A -> LIST A
cons a as = [ inr (a , as) ]

-- use fold to define concatenation

conc : {A : Set} -> LIST A -> LIST A -> LIST A
conc {A} xs ys = fold (kLIST A) ((\
                 { (inl _) -> ys
                 ; (inr (a , as)) -> cons a as
                 })) xs

-- prove the following (the yellow bits should disappear when
-- you define kLIST);
-- maddeningly, "rewrite" won't do it, but this piece of kit
-- (which is like a manual version of rewrite) will do it

cong : {S T : Set}(f : S -> T){a b : S} -> a == b -> f a == f b
cong f refl = refl

concNil : {A : Set}(as : LIST A) -> conc as nil == as
concNil [ inl <> ] = refl
concNil [ inr (outl , outr) ] = cong (cons outl) (concNil outr)

myas : LIST Nat
myas = cons 6 (cons 7  nil)

mybs : LIST Nat
mybs = cons 7 (cons 8 (cons 9 nil))

mycs : LIST Nat
mycs = cons 11 nil

concAssoc : {A : Set}(as bs cs : LIST A) ->
            conc (conc as bs) cs == conc as (conc bs cs)
concAssoc [ inl <> ] bs cs = refl
concAssoc [ inr (a , as) ] bs cs = cong (cons a) (concAssoc as bs cs)


{- 3.3 Trees in the Kit -}

-- give a kit code for binary trees with unlabelled leaves
-- and nodes labelled with elements of NAT

kTREE : Kit
kTREE = kK One k+ (kK NAT k* (kId k* kId))

TREE : Set
TREE = Data kTREE

-- give the constructors

leaf : TREE
leaf = [ inl <> ] 

node : TREE -> NAT -> TREE -> TREE
node l n r = [ inr (n , l , r) ]

-- implement flattening (slow flattening is ok) as a fold

flattenFold : One /+/ NAT /*/ LIST NAT /*/ LIST NAT -> LIST NAT
flattenFold (inl x) = nil
flattenFold (inr (n , l , r)) = conc l (cons n r)

flatten : TREE -> LIST NAT
flatten = fold kTREE flattenFold


{- 3.4 "rec" from "fold" -}

-- The recursor is a variation on the theme of fold, but you
-- get a wee bit more information at each step. In particular,
-- in each recursive position, you get the original substructure
-- *as well as* the value that is computed from it.

rec : (k : Kit){X : Set} ->

      (kFun k (Data k /*/ X) -> X) ->
--             ^^^^^^     ^
--       substructure   value

      Data k -> X

      -- Demonstrate that rec is no more powerful than fold by constructing
-- rec from fold. The trouble is that fold throws away the original
-- substructures. But you can compensate by computing something extra
-- as well as the value you actually want.

rec k {X} f d = outr (fold k {Data k /*/ X} (\ x -> ([ kitMap k outl x ] , (f x))) d)


-- use rec to implement "insList", being the function which inserts
-- a number in a list, such that if the input list is in increasing
-- order, so is the output list; you may assume that "lessEq" exists

lessEq : NAT -> NAT -> Two

insListRec : One /+/ NAT /*/ Data (kLIST NAT) /*/ LIST NAT -> NAT -> LIST NAT
insListRec (inl _) _ = nil
insListRec (inr (x , prev , insNPrev)) n = if lessEq n x  then cons n (cons x prev) else cons x insNPrev 


insList : NAT -> LIST NAT -> LIST NAT
insList n = rec (kLIST NAT) (\ x -> insListRec x n)

-- justify the assumption by defining "lessEq"; do not use explicit
-- recursion;
-- note that the thing we build for each number is its less-or-equal
-- test;
-- do use "rec kNat" once more to take numbers apart

lessEqHelp : One /+/ Data kNat /*/ (NAT -> Two) -> NAT -> Two
lessEqHelp (inl <>) _ = tt
lessEqHelp (inr (x , lessEqX)) y = rec kNat (\ { (inl <>) -> ff ; (inr ysuc) → lessEqX (outl ysuc) }) y

lessEq x y = rec kNat {NAT -> Two} lessEqHelp x y

-- implement insertion for binary search trees using "rec"

insTreeRec : One /+/ NAT /*/ (Data kTREE /*/ TREE) /*/ Data kTREE /*/ TREE -> NAT -> TREE
insTreeRec (inl <>) n = node leaf n leaf
insTreeRec (inr (h , (prevL , prevR) , nInPrevL , nInPrevR)) n = if lessEq n h then node nInPrevL h prevR else node prevL h nInPrevR
  -- prevR and nInPrevL are the wrong way wround in the patterns.

insTree : NAT -> TREE -> TREE
insTree n = rec kTREE (\ x -> insTreeRec x n)
