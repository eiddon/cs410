module Ex2 where --15/15

{-----------------------------------------------------------------------------
Name:
-----------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
CS410 Exercise 2, due 5pm on Monday of Week 6 (3 November 2014)
NOTE: I am well aware that week 6 is quite busy with deadlines,
what with CS408-related obligations and so on. I'd much prefer
you did things to the best of your ability rather than on time,
so I would be sympathetic to requests for some flexibility.
Still, your best bet is to make an early start rather than a
late finish.
-----------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
This exercise is based around extending Hutton's razor with Boolean
values and conditional expressions. By introducing a second value
type, we acquire the risk of type mismatch. The idea here is to
explore different approaches to managing that risk.
-----------------------------------------------------------------------------}

open import Ex1Prelude
open import Ex2Prelude

{- The extended Hutton's Razor syntax -}

data HExpIf : Set where
  num   : Nat -> HExpIf
  boo   : Two -> HExpIf
  _+++_ : HExpIf -> HExpIf -> HExpIf
  hif_then_else_ : HExpIf -> HExpIf -> HExpIf -> HExpIf

{- Note that an expression

   hif eb then ex1 else ex2

   makes sense only when
     * eb produces a Boolean value
     * ex1 and ex2 produce the same sort of value (numeric or Boolean)
-}

HValIf : Set
HValIf = Two /+/ Nat

{- We now have the risk of run time type errors. Let's introduce a type
for things which can go wrong. -}

data Error (E X : Set) : Set where
  ok : X -> Error E X
  error : E -> Error E X

{- 2.1 Add a constructor to the following datatype for each different
   kind of run time error that can happen. (Come back to this exercise
   when you're writing the evaluator in 2.3.) Make these error reports
   as informative as you can.
-}

data EvalError : Set where
  nan : EvalError
  nab : EvalError
  -- your constructors here

{- 2.2 Write a little piece of "glue code" to make it easier to manage
   errors. The idea is to combine error-prone process in *sequence*, where
   the second process can depend on the value produced by the first if it
   succeeds. The resulting process is, of course, also error-prone, failing
   as soon as either component fails.
-}

_>>=_ : {E S T : Set}
        -> Error E S          -- process which tries to get an S
        -> (S -> Error E T)   -- given an S, process which tries for a T
        -> Error E T          -- combined in sequence
ok x >>= s2et = s2et x
error x >>= s2et = error x

{- 2.3 Implement an evaluator for HExpIf. Be sure to add only numbers and
   to branch only on Booleans. Report type mismatches as errors. You should
   use _>>=_ to help with the propagation of error messages.
-}

natify : HValIf -> Error EvalError Nat
natify (inl x) = error nan
natify (inr x) = ok x

boolify : HValIf -> Error EvalError Two
boolify (inl x) = ok x
boolify (inr x) = error nab

eval : HExpIf -> Error EvalError HValIf
eval (num x) = ok (inr x)
eval (boo x) = ok (inl x)
eval (x +++ y) = eval x >>= \ xv -> 
                 natify xv >>= \ xn ->
                 eval y >>= \ yv ->
                 natify yv >>= \ yn ->
                 ok (inr (xn + yn))
eval (hif b then t else e) = eval b >>= \ bv ->
                             boolify bv >>= \ bb ->
                             if bb then eval t else eval e

{- Note that the type of eval is not specific about whether the value
   expected is numeric or Boolean. It may help to introduce auxiliary
   definitions of error-prone processes which are "ok" only for the
   type that you really want.
-}

{- Next up, stack machine code, and its execution. -}

data HBCode : Set where
  PUSHN : Nat -> HBCode
  PUSHB : Two -> HBCode
  ADD : HBCode
  _SEQ_ : HBCode -> HBCode -> HBCode
  _IFPOP_ : HBCode -> HBCode -> HBCode

{- The intended behaviour of (t IFPOP f) is as follows
  * pop the (we hope) Boolean value from top of stack
  * if it's tt, execute t, else execute f
  * whichever branch is executed, it gets the popped stack to start
-}

{- 2.4 Populate the type of possible execution errors and implement the
   execution behaviour of HBCode, operating on a stack represented as
   a list of HValIf values.
-}

data ExecError : Set where
  nab : ExecError
  nan : ExecError
  underflow : ExecError
  -- your constructors here

natify' : HValIf -> Error ExecError Nat
natify' (inl x) = error nan
natify' (inr x) = ok x

boolify' : HValIf -> Error ExecError Two
boolify' (inl x) = ok x
boolify' (inr x) = error nab

exec : HBCode -> List HValIf -> Error ExecError (List HValIf)
exec (PUSHN x) s = ok ((inr x) :> s)
exec (PUSHB x) s = ok ((inl x) :> s)
exec ADD (x :> y :> s) = natify' x >>=
                         (\ xn -> natify' y >>=
                         (\ yn -> ok ((inr (xn + yn)) :> s)))
exec ADD _ = error underflow
exec (a SEQ b) s = exec a s >>=
                   (\ saa -> exec b saa)
exec (t IFPOP f) [] = error underflow
exec (t IFPOP f) (x :> s) = boolify' x >>=
                            (\ xb -> if xb then exec t s else exec f s)

{- Next, we take a look at code generation and type safety. -}

data HTy : Set where  -- we have two types in HExpIf
  NUM BOOL : HTy

_=HTy=_ : HTy -> HTy -> Two   -- we can test if two types are equal
NUM  =HTy= NUM   = tt
NUM  =HTy= BOOL  = ff
BOOL =HTy= NUM   = ff
BOOL =HTy= BOOL  = tt

{- 2.5 Write a type-synthesizing compiler, computing both the HTy type and
   the HBCode executable for a given expression. Your compiler should
   give an informative error report if the expression it receives is
   ill typed. Your compiler should also ensure (at least informally) that
   the code produced will never trigger any execution errors.
-}

data CompileError : Set where
  nan : CompileError
  nab : CompileError
  neq : CompileError
  -- your constructors here

compile : HExpIf -> Error CompileError (HTy /*/ HBCode)
compile (num x) = ok (NUM , PUSHN x)
compile (boo x) = ok (BOOL , PUSHB x)
compile (e +++ f) = compile e >>=
                    uncurry (\ et ec -> compile f >>=
                    uncurry (\ ft fc ->
                    if et =HTy= NUM then
                      if ft =HTy= NUM then
                      ok (NUM , (ec SEQ (fc SEQ ADD) ))
                      else error nan
                    else error nan))
compile (hif b then t else e) = compile b >>=
                                uncurry (\ bt bc -> compile t >>=
                                uncurry (\ tt tc -> compile e >>=
                                uncurry (\ et ec ->
                                if bt =HTy= BOOL then
                                  if tt =HTy= et then
                                    ok (tt , (bc SEQ (tc IFPOP ec)))
                                    else error neq
                                  else error nab)))

{- You have a little bit more room for creative problem-solving in what's
   left of the exercise. The plan is to build the type system into expressions
   and code, the same way we did with plain Hutton's Razor in class.
-}

{- If we *know* which HTy type we want, we can compute which Agda type we
   expect our value to take. -}

HVal : HTy -> Set
HVal NUM  = Nat
HVal BOOL = Two

{- 2.6 Finish the type of typed expressions. You should ensure that only
   well HTy-typed expressions can be constructed. -}

data THExpIf : HTy -> Set where
  val : {t : HTy} -> HVal t -> THExpIf t
  _+++_ : THExpIf NUM ->  THExpIf NUM -> THExpIf NUM
  thif_then_else_ : {t : HTy} -> THExpIf BOOL -> THExpIf t -> THExpIf t -> THExpIf t

{- 2.7 Implement a type-safe evaluator. -}

teval : {t : HTy} -> THExpIf t -> HVal t
teval (val x) = x
teval (x +++ y) = teval x + teval y
teval (thif_then_else_ b t e) = if teval b then teval t else teval e

{- 2.8 Implement a type checker. -}

data TypeError : Set where
  nan : TypeError
  nab : TypeError
  -- your constructors here

tcheck : (t : HTy) -> HExpIf -> Error TypeError (THExpIf t)
tcheck NUM (num x) = ok (val x)
tcheck BOOL (num x) = error nab
tcheck NUM (boo x) = error nan
tcheck BOOL (boo x) = ok (val x)
tcheck NUM (x +++ y) = tcheck NUM x >>= (\ xe -> tcheck NUM y >>= \ ye -> ok (xe +++ ye))
tcheck BOOL (x +++ y) = error nab
tcheck ty (hif b then t else e) = tcheck BOOL b >>= (\ be -> tcheck ty t >>= \ te -> tcheck ty e >>= \ ee -> ok (thif_then_else_ be te ee))

{- 2.9 Adapt the technique from Hutton.agda to give a type-safe underflow-free
   version of HBCode. You will need to think what is a good type to represent
   the "shape" of a stack: before, we just used Nat to represent the *height* of
   the stack, but now we must worry about types. See next question for a hint. -}

Stk : List HTy -> Set
Stk xs = All HVal xs

data THBCode : List HTy -> List HTy -> Set where
  PUSHN : {i : List HTy} -> Nat ->
          THBCode i (NUM :> i)
  PUSHB : {i : List HTy} -> Two ->
          THBCode i (BOOL :> i)
  ADD : {i : List HTy} ->
        THBCode (NUM :> NUM :> i) (NUM :> i)
  _-SEQ-_ : {i j k : List HTy} -> THBCode i j -> THBCode j k ->
          THBCode i k
  _-IFPOP-_ : {i k : List HTy} ->
            THBCode i k -> THBCode i k ->
            THBCode (BOOL :> i) k

{- 2.10 Implement the execution semantics for your code. You will need to think
   about how to represent a stack. The Ex2Prelude.agda file contains a very
   handy piece of kit for this purpose. You write the type, too. -}

texec : {i j : List HTy} -> THBCode i j -> Stk i -> Stk j
texec (PUSHN n) s = n , s
texec (PUSHB b) s = b , s
texec ADD (x , y , s) = (x + y) , s  -- backwards but commutative
texec (x -SEQ- y) s = texec y (texec x s)
texec (t -IFPOP- _) (tt , s) = texec t s
texec (_ -IFPOP- e) (ff , s) = texec e s


{- 2.11 Write the compiler from well typed expressions to safe code. -}
{-
tcompile : {t : HTy} -> THExpIf t -> {!!}
tcompile e = {!!}
-}
tcompile : {t : HTy} -> {i : List HTy} -> THExpIf t -> THBCode i (t :> i)
tcompile {NUM} (val x) = PUSHN x
tcompile {BOOL} (val x) = PUSHB x
tcompile (x +++ y) = tcompile x -SEQ- (tcompile y -SEQ- ADD)
tcompile (thif t then e else b) = tcompile t -SEQ- (tcompile e -IFPOP- tcompile b)
