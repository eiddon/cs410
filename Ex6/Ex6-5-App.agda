module Ex6-5-App where

open import Ex6-Setup
open import Ex6-1-Vec
open import Ex6-2-Box
open import Ex6-3-Cut
open import Ex6-4-Dis


---------------------------------------------------------------------------
-- SMOOTHER AND MORE INTERESTING APPLICATIONS (5 marks)                  --
---------------------------------------------------------------------------

-- As these are the 5 marks which get you from "awesome first" to
-- "perfection", I'm leaving this task more open. There are two
-- parts to it.

-- Part 1. MINIMIZE FLICKER. The episode 4 setup repaints the whole
-- screenful every time. With a little bit of redesign, it's possible
-- to ensure that you repaint only what changes. Re-engineer the
-- functionality of exercise 4 to reduce flicker.
-- (2 marks)

-- Part 2. PUT SOMETHING INTERESTING IN THE RECTANGLES. It's one thing
-- being able to shove rectangles around the screen, but it would be
-- good if there was some actual content to them. You're free to design
-- whatever you like (and to raid Ex5 for spare parts, of course), but
-- I have some marking guidelines.
--   any nontrivial static content in the rectangles scores 1 mark,
--     provided you can still move and resize them;
--   treating the rectangle as a viewport into some content larger than
--     the rectangle scores 1 mark, provided you can move the viewport
--     all around the content
--   significant keyboard interaction with the rectangle at the front,
--     beyond what is needed for moving it, resizing it and refocusing
--     it, is worth 1 mark
-- (3 marks)

-- If you can think of other interesting things you might want to do that
-- don't quite fit the mark scheme for part 2, by all means pitch them to
-- me and I'll tell you how much I'd be willing to "pay" for them.


-- The rest of the file consists of some bits and pieces I came up with
-- while I was experimenting. You are free to adopt, adapt or reject these
-- components.


---------------------------------------------------------------------------
-- CROP-AND-PAD                                                          --
---------------------------------------------------------------------------

-- As with overlay, the types of cropPadLR and cropPadTB from episode 4
-- are more specific than is necessarily helpful. How about this instead?

cropPadXLR :  {X : Nat -> Nat -> Set}           -- some stuff
              (ck : CutKit X) ->                -- how to cut stuff
              (px : {w h : Nat} -> X w h) ->    -- how to make "blank" stuff
              (w h w' : Nat) -> Box X w h -> Box X w' h
cropPadXLR ck px w h w' b with compare w w'
cropPadXLR ck px w h .w b | onJoin refl refl = b
cropPadXLR ck px w h .(w + r) b | left r refl _ = leri w b r [ px ] refl
cropPadXLR ck px .(w' + r) h w' b | right r refl _ = fst (CutKit.cutLR (boxCutKit ck) (w' + r) h w' r refl b)

cropPadXTB : {X : Nat -> Nat -> Set}
             (ck : CutKit X) ->
             (px : {w h : Nat} -> X w h) ->
             (w h h' : Nat) -> Box X w h -> Box X w h'
cropPadXTB ck px w h h' b with compare h h'
cropPadXTB ck px w h .h b | onJoin refl refl = b
cropPadXTB ck px w h .(h + r) b | left r refl _ = tobo h b r [ px ] refl
cropPadXTB ck px w .(h' + r) h' b | right r refl _ = fst (CutKit.cutTB (boxCutKit ck) w (h' + r) h' r refl b)

cropPadX : {X : Nat -> Nat -> Set}
           (w h : Nat)(w' h' : Nat) ->
           CutKit X ->
           (px : {w h : Nat} -> X w h) ->
           Box X w h -> Box X w' h'
cropPadX w h w' h' ck px x = cropPadXTB ck px w' h h' (cropPadXLR ck px w h w' x)


-- and its TB friend, of course. Now you can crop things other than
-- Paintings.



---------------------------------------------------------------------------
-- GENERALIZING OVERLAY TO MASKING                                       --
---------------------------------------------------------------------------

-- Here's what I should have asked you to build in episode 3.

mask : {X Y Z : Nat -> Nat -> Set} -> CutKit Y ->
       ({w h : Nat} -> X w h -> Box Y w h -> Box Z w h) ->
       {w h : Nat} ->
       {- front -}     Box X w h ->
       {- back  -}     Box Y w h ->
       {- combined -}  Box Z w h
mask {X}{Y}{Z} ck m = go where
  open CutKit (boxCutKit ck)
  go : {w h : Nat} -> Box X w h -> Box Y w h -> Box Z w h
  go [ x ] yb = m x yb
  go {w = .(wl + wr)} {h = h} (leri wl xbl wr xbr refl) yb with cutLR (wl + wr) h wl wr refl yb
  go (leri wl xbl wr xbr refl) yb | ybl , ybr = leri wl (go xbl ybl) wr (go xbr ybr) refl
  go {w = w} {h = .(ht + hb)} (tobo ht xbt hb xbb refl) yb with cutTB w (ht + hb) ht hb refl yb
  go (tobo ht xbt hb xbb refl) yb | ybt , ybb = tobo ht (go xbt ybt) hb (go xbb ybb) refl

-- The idea, as with "overlay", is that the box of X stuff is in front
-- and the box of Y stuff is behind. You need to combine them to make
-- a box of Z stuff. Fortunately, once you've cut your way through the
-- structure to reach each basic X tile, the parameter m is just what
-- you need to know to combine that tile with the Y box to make a Z box.

-- You should find that the implementation is almost the same as "overlay"
-- but the extra generality is quite useful. You might find the following
-- concepts helpful.

Update : Nat -> Nat -> Set
Update w h
  =    Two                               -- does it demand repainting?
  /*/  HoleOr (Matrix ColourChar) w h    -- transparent or opaque stuff?

-- A Box Update is more informative than a Painting, in that every primitive
-- tile tells you whether it is "new" or not; now you can build the logic
-- to combine the changes which happen in each layer. In particular, you can
-- model the idea that moving or resizing in the front can reveal stuff which
-- used to be hidden at the back: that's a "new hole", and even if what's
-- behind it hasn't changed, you'll need to update the display.

Selection : Nat -> Nat -> Set
Selection = Box \ _ _ -> Two

-- A selection is a tiling of the rectangle with tt or ff. You can lift all
-- the usual logical operations to selections using mask and mapBox. For
-- example, XOR-ing selections gives you everything in one but not the other,
-- which might help you spot what has changed.


---------------------------------------------------------------------------
-- RUN-LENGTH ENCODING INACTIVITY                                        --
---------------------------------------------------------------------------

-- An idea which might be useful in helping you to manage more selective
-- redrawing is to build the idea of "doing nothing for a bit" into the
-- lists of actions that you build up. Think about updating one line of
-- the display: it might sometimes help to be able to say "keep the next
-- n cells as they were", which you could interpret as a cursor move, rather
-- than a text output.

-- Correspondingly, you might benefit from a data structure like this:

data Skippy (X : Set) : Set where
  []    : Skippy X                        -- stop
  _::_  : X -> Skippy X -> Skippy X       -- give one X then keep going
  _>_   : Nat -> Skippy X -> Skippy X     -- skip n places, then keep going

-- Now, when you work with these structures, you should enforce that
--   (i)  you never have   zero > xs    (if there's nothing to skip, don't)
--   (ii) you never have   m > n > xs   (don't skip twice, skip further)

-- It's possible to refine the type Skippy to enforce those properties by
-- the power of type checking. Or you could just make sure you never cheat
-- by the more traditional method of defining a "smart constructor"

_>>_ : {X : Set} -> Nat -> Skippy X -> Skippy X
zero >> xs = xs
suc n >> (x > xs) = (suc n + x) > xs
suc n >> (xs) = suc n > xs


-- The definition I've given you isn't very smart: >> is the same as >.
-- But the idea is that you add some special cases before that last line
-- which catch the possibilities that should never happen and do something
-- else instead. Now you can define operations like concatenation, using
-- the smart constructor in place of the regular one.

_+>+_ : {X : Set} -> Skippy X -> Skippy X -> Skippy X
[]         +>+  ys  = ys
(x :: xs)  +>+  ys  = x :: (xs +>+ ys)
(m > xs)   +>+  ys  = m >> (xs +>+ ys)
--                      ^^ see?

-- That way, you know that if the lists you're concatenating satisfy the
-- above rules, so will the result.


---------------------------------------------------------------------------
-- ANOTHER SMART CONSTRUCTOR                                             --
---------------------------------------------------------------------------

-- You might consider playing the same game with the List Action type that
-- we use for displaying things. Here's part of mine.

_:a:_ : Action -> List Action -> List Action
--Eliminate double moves
goRowCol _ _ :a: (goRowCol r c :: as) = goRowCol r c :a: as
move _ _ :a: (goRowCol r c :: as) = goRowCol r c :a: as

{- Always do lateral moves first.
   My thinking is that this will collapse down, right, down , right chains.
-}
move up mov :a: (move left mov' :: as) = move left mov' :: (move up mov :a: as)
move up mov :a: (move right mov' :: as) = (move right mov') :: (move up mov :a: as)

move down mov :a: (move left mov' :: as) = move left mov' :: (move down mov :a: as)
move down mov :a: (move right mov' :: as) = move right mov' :: (move down mov :a: as)

--Merge moves up and down...
move up mov :a: (move up mov' :: as) = move up (mov + mov') :a: as
move up mov :a: (move down mov' :: as) with compare mov mov'
move up mov :a: (move down .mov :: as) | onJoin refl refl = as
move up mov :a: (move down .(mov + r) :: as) | left r refl _ = (move down r) :a: as
move up .(mov' + r) :a: (move down mov' :: as) | right r refl _ = (move up r) :a: as

move down mov :a: (move down mov' :: as) = (move down (mov + mov')) :a: as
move down mov :a: (move up mov' :: as) with compare mov mov'
move down mov :a: (move up .mov :: as) | onJoin refl refl = as
move down mov :a: (move up .(mov + r) :: as) | left r refl _ = (move up r) :a: as
move down .(mov' + r) :a: (move up mov' :: as) | right r refl _ = (move down r) :a: as

--... and left and right
move left mov :a: (move left mov' :: as) = (move left (mov + mov')) :a: as
move left mov :a: (move right mov' :: as) with compare mov mov'
move left mov :a: (move right .mov :: as) | onJoin refl refl = as
move left mov :a: (move right .(mov + r) :: as) | left r refl _ = (move right r) :a: as
move left .(mov' + r) :a: (move right mov' :: as) | right r refl _ = (move left r) :a: as

move right mov :a: (move right mov' :: as) = (move right (mov + mov')) :a: as
move right mov :a: (move left mov' :: as) with compare mov mov'
move right mov :a: (move left .mov :: as) | onJoin refl refl = as
move right mov :a: (move left .(mov + r) :: as) | left r refl _ = (move left r) :a: as
move right .(mov' + r) :a: (move left mov' :: as) | right r refl _ = (move right r) :a: as

--Move a bit further or a bit less
goRowCol row col :a: (move down mov :: as) = (goRowCol (row + mov) col) :a: as
goRowCol row col :a: (move right mov :: as) = (goRowCol row (col + mov)) :a: as

goRowCol row col :a: (move up mov :: as) with compare col mov
goRowCol row col :a: (move up .col :: as) | onJoin refl refl = (goRowCol row 0) :a: as
goRowCol row col :a: (move up .(col + r) :: as) | left r refl _ = (goRowCol row 0) :a: as
goRowCol row .(mov + r) :a: (move up mov :: as) | right r refl _ = (goRowCol row r) :a: as

goRowCol row col :a: (move left mov :: as) with compare col mov
goRowCol row col :a: (move left .col :: as) | onJoin refl refl = goRowCol row 0 :a: as
goRowCol row col :a: (move left .(col + r) :: as) | left r refl _ = (goRowCol row 0) :a: as
goRowCol row .(mov + r) :a: (move left mov :: as) | right r refl _ = goRowCol row mov :a: as

--Send both bits of text at once.
sendText x :a: (sendText t :: as) = sendText (x +-+ t) :a: as

--Set the colour then set it again, nope. (Debatable performance benefit - pattern match)
fgText _ :a: (fgText c :: as) = fgText c :a: as
bgText _ :a: (bgText c :: as) = bgText c :a: as

--Otherwise these ops are non-combinable
a :a: as = a :: as

_+a+_ : List Action -> List Action -> List Action
[] +a+ ys = ys
(x :: xs) +a+ ys = x :a: (xs +a+ ys)
infixr 6 _+a+_

-- That's to say
--   (i)  there's no point in positioning the cursor twice in a row, when
--          the second just overrides the first;
--   (ii) don't send two small texts when you can send one big text.



---------------------------------------------------------------------------
-- FROM APPLICATIONS TO UPPLICATIONS                                     --
---------------------------------------------------------------------------

-- The notion of "Application" from episode 4 required you to define a
-- paintMe function which just gives the full display. You may need to
-- rethink this concept to get an *updating* application, or "Upplication"
-- in which at least some of the event handlers tell you just what has
-- changed. You will certainly need *more* information, but you should
-- also consider whether it would be better if the existing information
-- took a different form.

record Upplication (S : Nat -> Nat -> Set) : Set where
  field
    handleKey    : Key -> S []> (λ w h → S w h /*/ Selection w h) -- Give me the new state and what, if anything, changed.
    handleResize : {w h : Nat}(w' h' : Nat) -> S w h -> S w' h'
    paintMe      : (λ w h → S w h /*/ Selection w h) []> Box Update
    cursorMe     : {w h : Nat} -> S w h -> Nat /*/ Nat -- x,y coords

lazyApp2Upp : {S : Nat -> Nat -> Set} -> Application S -> Upplication S
lazyApp2Upp app = record
                   { handleKey    = λ k s → Application.handleKey app k s , [ tt ]
                   ; handleResize = Application.handleResize app
                   ; paintMe      = λ { (s , sel) → mapBox (λ x → tt , x) (Application.paintMe app s) }
                   ; cursorMe     = Application.cursorMe app
                   }

paintUpdate : {w h : Nat} -> Box Update w h -> List Action
paintUpdate [ tt , stuff ] = paintAction (paintMatrix [ stuff ])
paintUpdate {w} {h} [ ff , _ ] = move down h :a: []
paintUpdate {w} {h} (leri wl le wr ri x) = paintUpdate le +a+ move up h :a: (move right wl :a: paintUpdate ri +a+ move left wl :: [])
paintUpdate (tobo ht to hb bo x) = paintUpdate to +a+ paintUpdate bo

open Upplication
--TODO:Actually do some painting
uppPaint : {S : Nat -> Nat -> Set}{w h : Nat} ->
           Upplication S -> S w h -> Selection w h -> List Action
uppPaint upp s sel with paintMe upp (s , sel)
uppPaint upp s sel | res = goRowCol 0 0 :a: (paintUpdate res +a+ goRowCol (fst xy) (snd xy) :: [])
  where
    xy = cursorMe upp s
uppHandler : {S : Nat -> Nat -> Set} ->
             Upplication S ->
             Event -> AppState S -> AppState S /**/ List Action
uppHandler {S} upp (key k) (w , h , s) with handleKey upp k s
uppHandler upp (key k) (w , h , s) | s' , sel = (w , h , s') , uppPaint upp s' sel
uppHandler upp (resize w' h') (w , h , s) with handleResize upp w' h' s
... | s' = (w' , h' , s') , uppPaint upp s' [ tt ]

uppMain : {S : Nat -> Nat -> Set} -> Upplication S -> S 0 0 -> IO Thud
uppMain upp s = mainLoop (0 , 0 , s) (uppHandler upp)

---------------------------------------------------------------------------
-- PUTTING UPPLICATIONS TOGETHER                                         --
---------------------------------------------------------------------------

-- In episode 4, you had to define the frontBack operator, which combined
-- two applications into one by *layering* them. How does that work for
-- upplications? What other spatial combinations of upplications make sense?
-- Here are three possibilities worth considering:
--   (i)    putting two applications side-by-side,
--   (ii)   putting one application above another,
--   (iii)  viewing an application through a rectangular viewport.
-- Of course, there are many more possibilities.
--
-- Toggling with the tab key is all very well when there are only two
-- components, but you might need to think a little harder about how to
-- navigate when there are more. Normally, you'd use your finger for that,
-- or a mouse. Sadly, I don't know how to organise mouse interaction with
-- terminal windows. But you could make a pretend mouse: a top layer which
-- displays a "mouse cursor" that you can move around with arrow keys when
-- you're in "mouse mode". Clicking could be a keystroke which activates
-- the frontmost opaque thing behind the mouse and exits mouse mode.


---------------------------------------------------------------------------
-- MAIN                                                                  --
---------------------------------------------------------------------------

-- I've added
--   make go5
-- to the Makefile

upCut : CutKit (\ _ _ -> Two)
upCut = record { cutLR = λ { _ _ _ _ _ t → t , t } ; cutTB = λ _ _ _ _ _ t → t , t }

maskRectSel : {w h : Nat} -> HoleOr (Matrix ColourChar) w h -> Box (\ _ _ -> Two) w h -> Box Update w h
maskRectSel r [ x ] = [ (x , r) ]
maskRectSel {.(wl + wr)} {h} r (leri wl sl wr sr refl) with CutKit.cutLR (holeCutKit matrixCutKit) (wl + wr) h wl wr refl r
maskRectSel r (leri wl sl wr sr refl) | rl , rr = leri wl (maskRectSel rl sl) wr (maskRectSel rr sr) refl
maskRectSel {w} {.(ht + hb)} r (tobo ht st hb sb refl) with CutKit.cutTB (holeCutKit matrixCutKit) w (ht + hb) ht hb refl r
maskRectSel r (tobo ht st hb sb refl) | rt , rb = tobo ht (maskRectSel rt st) hb (maskRectSel rb sb) refl

rectPaint' : {w h : Nat} -> Colour -> Rect -> Painting w h
rectPaint' {w} {h} c (rect rw rh rm dm) = cropPadLR (rm + rw) h w (cropPadTB (rm + rw) (dm + rh) h (tobo dm [ Hole ] rh [ [ (vec (vec (c - ' ' / c))) ] ] refl))

rectPaintMe : Colour -> (w h : Nat) -> Rect -> Selection w h -> Box Update w h
rectPaintMe c w h r s = mask upCut maskRectSel (rectPaint c r) s

rec2Sel : {w h : Nat} -> Rect -> Selection w h
rec2Sel {w} {h} (rect rw rh rm dm) = cropPadX
  (rm + suc (suc rw))
  (dm + suc (suc rh))
  w
  h
  upCut
  ff
  (tobo dm [ ff ] (suc (suc rh)) (leri rm [ ff ] (suc (suc rw)) [ tt ] refl) refl)


xor : Two -> Two -> Two
xor tt tt = ff
xor tt ff = tt
xor ff tt = tt
xor ff ff = ff

or : Two -> Two -> Two
or tt _ = tt
or ff b = b

and : Two -> Two -> Two
and tt b = b
and ff _ = ff

logic : {w h : Nat} -> (Two -> Two -> Two) -> Two -> Box (\ _ _ -> Two) w h -> Box (\ _ _ -> Two) w h
logic f t = mapBox (f t)

recOverRec : {w h : Nat} -> Rect -> Rect -> (Two -> Two -> Two) -> Selection w h
recOverRec r r' f = mask upCut (logic f) (rec2Sel r) (rec2Sel r')

ruHandleKey : Key -> (\w h -> Rect) []> (\ w h -> Rect /*/ Selection w h)
ruHandleKey (arrow normal up) (rect rw rh rm (suc dm)) = (rect rw rh rm dm) , recOverRec (rect rw rh rm (suc dm)) (rect rw rh rm dm) or
ruHandleKey (arrow normal down) (rect rw rh rm dm) = rect rw rh rm (suc dm) ,  recOverRec (rect rw rh rm dm) (rect rw rh rm (suc dm)) or
ruHandleKey (arrow normal left) (rect rw rh (suc rm) dm) = (rect rw rh rm dm) , recOverRec (rect rw rh (suc rm) dm) (rect rw rh rm dm) or
ruHandleKey (arrow normal right) (rect rw rh rm dm) = rect rw rh (suc rm) dm , recOverRec (rect rw rh rm dm) (rect rw rh (suc rm) dm) or
ruHandleKey (arrow shift up) (rect rw (suc rh) rm dm) = (rect rw rh rm dm) , recOverRec (rect rw (suc rh) rm dm) (rect rw rh rm dm) or
ruHandleKey (arrow shift down) (rect rw rh rm dm) = (rect rw (suc rh) rm dm) , recOverRec (rect rw rh rm dm) (rect rw (suc rh) rm dm) or
ruHandleKey (arrow shift left) (rect (suc rw) rh rm dm) = rect rw rh rm dm , recOverRec (rect (suc rw) rh rm dm) (rect rw rh rm dm) or
ruHandleKey (arrow shift right) (rect rw rh rm dm) = rect (suc rw) rh rm dm , recOverRec (rect rw rh rm dm) (rect (suc rw) rh rm dm) or
ruHandleKey _ r = r , [ ff ]

rectUpp : Colour -> Upplication \ w h -> Rect
rectUpp c = record
              { handleKey    = ruHandleKey
              ; handleResize = λ w' h' x → x
              ; paintMe      = λ { {w} {h} (rec , sel) → rectPaintMe c w h rec sel }
              ; cursorMe     = λ { (rect rw rh rm dm) → suc rh + dm , suc rw + rm }
              }

fbUHandleKey : {S T : Nat -> Nat -> Set} -> Upplication S -> Upplication T -> Key -> (\w h -> FrontBack S T w h) []> (\w h -> FrontBack S T w h /*/ Selection w h)
fbUHandleKey uppS uppT tab (fb f b) = bf b f , [ tt ]
fbUHandleKey uppS uppT tab (bf b f) = fb f b , [ tt ]
fbUHandleKey uppS uppT k (fb f b) with handleKey uppS k f
fbUHandleKey uppS uppT k (fb f b) | f' , sel = (fb f' b) , sel
fbUHandleKey uppS uppT k (bf b f) with handleKey uppT k b
fbUHandleKey uppS uppT k (bf b f) | b' , sel = (bf b' f) , sel

fbUHandleResize : {S T : Nat -> Nat -> Set} -> Upplication S -> Upplication T -> {w h : Nat}(w' h' : Nat) -> FrontBack S T w h -> FrontBack S T w' h'
fbUHandleResize uppS uppT w' h' (fb s t) = fb (handleResize uppS w' h' s) (handleResize uppT w' h' t)
fbUHandleResize uppS uppT w' h' (bf t s) = bf (handleResize uppT w' h' t) (handleResize uppS w' h' s)

updCutLR : (w h wl wr : Nat) -> wl + wr == w -> Update w h -> Update wl h /*/ Update wr h
updCutLR .(wl + wr) h wl wr refl (tf , stuff) with CutKit.cutLR (holeCutKit matrixCutKit) (wl + wr) h wl wr refl stuff
updCutLR .(wl + wr) h wl wr refl (tf , stuff) | l , r = (tf , l) , tf , r

updCutTB : (w h ht hb : Nat) -> ht + hb == h -> Update w h -> Update w ht /*/ Update w hb
updCutTB w .(ht + hb) ht hb refl (tf , stuff) with CutKit.cutTB (holeCutKit matrixCutKit) w (ht + hb) ht hb refl stuff
updCutTB w .(ht + hb) ht hb refl (tf , stuff) | t , b = (tf , t) , tf , b

updCut : CutKit Update
updCut = record { cutLR = updCutLR
                ; cutTB = updCutTB
                }

someFunc : {w h : Nat} -> Update w h -> Box Update w h -> Box Update w h
someFunc (tf , Hole) [ tf' , stuff ] = [ or tf tf' , stuff ]
someFunc (tf , [ x ]) [ tf' , stuff ] = [ tf , [ x ] ]
someFunc {.(wl + wr)} {h} u (leri wl l wr r refl)  with CutKit.cutLR updCut (wl + wr) h wl wr refl u
someFunc u (leri wl l wr r refl) | ul , ur = leri wl (someFunc ul l) wr (someFunc ur r) refl
someFunc {w} {.(ht + hb)} u (tobo ht t hb b refl) with CutKit.cutTB updCut w (ht + hb) ht hb refl u
someFunc u (tobo ht t hb b refl) | ut , ub = tobo ht (someFunc ut t) hb (someFunc ub b) refl

upOverUp : {w h : Nat} -> Box Update w h -> Box Update w h -> Box Update w h
upOverUp b b' = mask updCut someFunc b b'

frontBackUpp : {S T : Nat -> Nat -> Set} ->
  Upplication S ->
  Upplication T ->
  Upplication \ w h -> FrontBack S T w h
frontBackUpp uppS uppT = record
  { handleKey    = fbUHandleKey uppS uppT --fbUHandleKey uppS uppT
  ; handleResize = fbUHandleResize uppS uppT
  ; paintMe      = λ { (fb f b , sel) -> upOverUp (paintMe uppS (f , sel)) (paintMe uppT (b , sel))
                     ; (bf b f , sel) -> upOverUp (paintMe uppT (b , sel)) (paintMe uppS (f , sel))
                     }
  ; cursorMe     = λ { (fb f b) → cursorMe uppS f ; (bf b f) → cursorMe uppT b }
  }

-- I've set the main application to be the silly one from episode 4, but
-- you can swap in your own thing.
main : IO Thud
--main = uppMain (rectUpp blue) (rect 5 5 10 10)
main = uppMain (frontBackUpp (rectUpp blue) (rectUpp red)) (fb (rect 10 40 3 15) (rect 20 40 8 15))
--main = uppMain (lazyApp2Upp (frontBack (rectApp blue) (rectApp red))) (fb (rect 10 40 3 15) (rect 20 40 8 15))

