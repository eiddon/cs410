module Edit where

{- This is the file where you should work. -}

open import AgdaSetup

{- The key editor data structure is the cursor. A Cursor M X represents
   being somewhere in the middle of a sequence of X values, holding an M. -}

record Cursor (M X : Set) : Set where
  constructor _<[_]>_
  field
    beforeMe  : Bwd X
    atMe      : M
    afterMe   : List X
infix 4 _<[_]>_

{- An editor buffer is a nested cursor: we're in the middle of a bunch of
   *lines*, holding a cursor for the current line, which puts us in the
   middle of a bunch of characters, holding the element of One. -}
Buffer : Set
Buffer = Cursor (Cursor One Char) (List Char)

{- This operator, called "chips", shuffles the elements from a backward list
   on to the start of a forward list, keeping them in the same order. -}
_<>>_ : {X : Set} -> Bwd X -> List X -> List X
[]         <>> xs  = xs
(xz <: x)  <>> xs  = xz <>> (x :> xs)

{- The "fish" operator goes the other way. -}
_<><_ : {X : Set} -> Bwd X -> List X -> Bwd X
xz <>< []         = xz
xz <>< (x :> xs)  = (xz <: x) <>< xs

{- You can turn a buffer into a list of lines, preserving its text. -}
bufText : Buffer -> List (List Char)
bufText
  (sz <[
   cz <[ <> ]> cs
   ]> ss)
  = sz <>> ((cz <>> cs) :> ss)

{- Here's an example of a proof of a fact about fish and chips. -}
firstFishFact : {X : Set} -> (xz : Bwd X)(xs : List X) ->
  (xz <>< xs) <>> []  ==  xz <>> xs
firstFishFact xz []          = refl
firstFishFact xz (x :> xs)   = firstFishFact (xz <: x) xs

secondFishFact : {X : Set}{a : Bwd X} -> (xs : List X) ->
  (a <>> xs) == (a <>< xs) <>> []
secondFishFact [] = refl
secondFishFact (x :> xs) = secondFishFact xs


fst : {X Y : Set} -> (X /*/ Y) -> X
fst (x , _) = x

snd : {X Y : Set} -> (X /*/ Y) -> Y
snd (_ , y) = y

{- You will need more such facts. -}

{- EXERCISE 5.1 -}
{- When we start the editor with the command
      ./Edit foo.txt
   the contents of foo.txt will be turned into a list of lines.
   Your (not so tricky) mission is to turn the file contents into a buffer which
   contains the same text.
   (1 mark)
-} -- 1/1
initBuf : List (List Char) -> Buffer
initBuf [] = [] <[ [] <[ <> ]> [] ]> []
initBuf (s :> ss) =
  [] <[ [] <[ <> ]> s ]> ss
{- As you can see, the current version will run, but it always gives the empty
   buffer, which is not what we want unless the input is empty. -}

{- Next comes the heart of the editor. You get a keystroke and the current buffer,
   and you have to say what is the new buffer. You also have to say what is the
   extent of the change.

   The tricky part is this: you have to be honest enough about your change
   report, so that we don't underestimate the amount of updating the screen needs.
-}

Honest : Buffer -> Change /*/ Buffer -> Set
Honest b (allQuiet    , b')                            = b == b'
Honest b (cursorMove  , b')                            = bufText b == bufText b'
Honest (sz <[ _ ]> ss) (lineEdit , (sz' <[ _ ]> ss'))  = (sz == sz') /*/ (ss == ss')
Honest _ (bigChange   , _)                             = One

record UpdateFrom (b : Buffer) : Set where   -- b is the starting buffer
  constructor _///_
  field
    update  : Change /*/ Buffer   -- change and new buffer
    honest  : Honest b update
open UpdateFrom
infix 2 _///_

_o_ : {X Y Z : Set} -> (X -> Y) -> (Y -> Z) -> (X -> Z)
(f o g) a = g (f a)
infixr 2 _o_

{--TODO: Generic--}
pad : {X : Set} -> Nat -> List X -> X -> List X
pad zero [] _ = []
pad (suc n) [] x = x :> pad n [] x
pad zero cs x = cs
pad (suc n) (c :> cs) x = c :> pad n cs x

padSp : Nat -> List Char -> List Char
padSp n l = pad n l ' '

length : {X : Set} -> List X -> Nat
length [] = zero
length (x :> xs) = suc (length xs)

forwards : {X : Set} -> Bwd X -> List X
forwards x = x <>> []

backwards : {X : Set} -> List X -> Bwd X
backwards x = [] <>< x

asBwd : {X Y : Set} -> (Bwd X -> Y) -> List X -> Y
asBwd f = backwards o f

asFwd : {X Y : Set} -> (List X -> Y) -> Bwd X -> Y
asFwd f = forwards o f

blength : {X : Set} -> Bwd X -> Nat
blength = forwards o length

map : {X Y : Set} -> (X -> Y) -> List X -> List Y
map f [] = []
map f (x :> xs) = f x :> map f xs

take : {X : Set} -> Nat -> List X -> List X
take _ [] = []
take zero _ = []
take (suc n) (x :> xs) = x :> take n xs

drop : {X : Set} -> Nat -> List X -> List X
drop _ [] = []
drop zero xs = xs
drop (suc n) (x :> xs) = drop n xs

_-_ : Nat -> Nat -> Nat
0 - _ = 0
x - 0 = x
suc x - suc y = x - y

splitFishFact : {X : Set}{a : Bwd X} -> (s : List X)(n : Nat) ->
  (a <>> s) == ((a <>< take n s) <>> (drop n s))
splitFishFact [] zero = refl
splitFishFact (x :> s) zero = refl
splitFishFact [] (suc n) = refl
splitFishFact (x :> s) (suc n) = splitFishFact s n

{--
takeDropFact : {X : Set} -> (s : List X)(n : Nat) ->
  s == take n s ++ drop n s
takeDropFact [] n = refl
takeDropFact (x :> s) zero = refl
takeDropFact xs n = {!!}
--}

{-# NO_TERMINATION_CHECK #-}
toNext : Char -> (List Char) -> Nat
toNext c [] = 0
toNext c (x :> cs) = if primCharEquality c x then 1 else (suc (toNext c cs))

{- EXERCISE 5.2 -}
{- Implement the appropriate behaviour for as many keystrokes as you can.
   I have done a couple for you, but I don't promise to have done them
   correctly. -}
--FIXME:cursorMove for up, down.
keystroke : Key -> (b : Buffer) -> UpdateFrom b
keystroke (char c) -- 1/1
  (sz <[
   cz <[ <> ]> cs
   ]> ss)
  = lineEdit ,
  (sz <[
   cz <: c <[ <> ]> cs
   ]> ss)
  /// refl , refl          -- see? same above and below
keystroke (arrow control right) (sz <[ cz <[ <> ]> cs ]> ss) = cursorMove , ((sz <[ cz <>< (take next cs) <[ <> ]> (drop next cs) ]> ss)) /// within ((\x -> (sz <>> (x :> ss)))) turn cz <>> cs into (cz <>< take next cs) <>> drop next cs because splitFishFact cs next
                                  where next = toNext ' ' cs
-- l&r 2/2
keystroke (arrow normal right) (sz <[ cz <[ <> ]> [] ]> s :> ss) = cursorMove , ((sz <: (cz <>> []) <[ [] <[ <> ]> s ]> ss)) /// refl
keystroke (arrow normal right) (sz <[ cz <[ <> ]> c :> cs ]> ss) = cursorMove , (sz <[ cz <: c <[ <> ]> cs ]> ss) /// refl
keystroke (arrow normal right) (sz <[ cz <[ <> ]> [] ]> []) = allQuiet , (sz <[ cz <[ <> ]> [] ]> []) /// refl
keystroke (arrow normal left) (sz <[ cz <: c <[ <> ]> cs ]> ss) = cursorMove , (sz <[ cz <[ <> ]> c :> cs ]> ss) /// refl
keystroke (arrow normal left) ([] <[ [] <[ <> ]> cs ]> ss) = cursorMove , ([] <[ [] <[ <> ]> cs ]> ss) /// refl
keystroke (arrow normal left) (sz <: s <[ [] <[ <> ]> cs ]> ss) = cursorMove , ((sz <[ ([] <>< s) <[ <> ]> [] ]> cs :> ss)) /// within ((\x -> sz <>> (x :> cs :> ss))) turn s into ([] <>< s) <>> [] because secondFishFact s

-- u&d 2/2
keystroke (arrow normal down) (sz <[ cz <[ <> ]> cs ]> s :> ss) = cursorMove ,
          (sz <: (cz <>> cs)
            <[ backwards (take (blength cz) s)
            <[ <> ]>
            drop (blength cz) s ]>
          ss)
          /// within (\x -> (sz <>> (cz <>> cs :> x :> ss))) turn s into backwards (take (blength cz) s) <>> drop (blength cz) s because splitFishFact  s (blength cz)
keystroke (arrow normal up) (sz <: s <[ cz <[ <> ]> cs ]> ss) = cursorMove ,
          ((sz
            <[ backwards (take (blength cz) s)
            <[ <> ]>
            drop (blength cz) s ]>
           (cz <>> cs) :> ss))
          /// within (\x -> sz <>> (x :> cz <>> cs :> ss)) turn s into backwards (take (blength cz) s) <>> drop (blength cz) s because splitFishFact s (blength cz)
-- bs&del 2/2
keystroke backspace (sz <: s <[ [] <[ <> ]> cs ]> ss) = bigChange ,
          (sz
          <[ backwards s
          <[ <> ]>
          cs ]>

          ss)
          ///
          <>
keystroke backspace (sz <[ cz <: x <[ <> ]> cs ]> ss) = lineEdit , (sz <[ cz <[ <> ]> cs ]> ss) /// refl , refl
keystroke delete (sz <[ cz <[ <> ]> x :> cs ]> ss) = lineEdit , (sz <[ cz <[ <> ]> cs ]> ss) /// refl , refl
keystroke delete (sz <[ cz <[ <> ]> [] ]> s :> ss) = bigChange , (sz <[ cz <[ <> ]> s ]> ss) /// <>

-- 2/2
keystroke enter (sz <[ cz <[ <> ]> cs ]> ss) = bigChange , ((sz <: (cz <>> []) <[ [] <[ <> ]> cs ]> ss)) /// <>
keystroke k b = allQuiet , b /// refl
{- Please expect to need to invent extra functions, e.g., to measure where you
   are, so that up and down arrow work properly. -}
{- Remember also that you can always overestimate the change by saying bigChange,
   which needs only a trivial proof. But you may find that the display will flicker
   badly if you do. -}
{- (char c)                 1 mark
   enter                    2 marks
   backspace delete         2 marks for the pair
   left right               2 marks for the pair (with cursorMove change)
   up down                  2 marks for the pair (with cursorMove change)
   -}

ensureWidth : (Nat /*/ Nat) -> List Char -> List Char
ensureWidth (w , c) = drop c o take w o padSp w

selectRegion : (Nat /*/ Nat) -> (Nat /*/ Nat) -> List (List Char) -> List (List Char)
selectRegion (w , h) (t , l) b = (map (ensureWidth (w , l)) (pad h (take h (drop t b)) (padSp w [])))

selectLine : Buffer -> List Char
selectLine (_ <[ cz <[ <> ]> cs ]> _) = cz <>> cs

{--drawRegion : List (List Char) -> (Nat /*/ Nat) -> List Action -> List Action
drawRegion [] _ as = as
drawRegion (s :> ss) (t , l) as = goRowCol t l :> sendText s :> drawRegion ss (suc t , l) as--}

drawRegion : List (List Char) -> Nat -> Nat -> List Action -> List Action
drawRegion [] _ _ as = as
drawRegion (s :> ss) acc l as = goRowCol acc 0 :> sendText s :> drawRegion ss (suc acc) l as

flatten : {X : Set} -> (List (List X)) -> List X
flatten [] = []
flatten (x :> xs) = x ++ flatten xs

whereami : (Nat /*/ Nat) -> Buffer -> (Nat /*/ Nat)
whereami (t , l) (sz <[ cz <[ <> ]> _ ]> _) = (blength sz - t) , (blength cz - l)

_>_ : Nat -> Nat -> Two
zero > y = ff
_ > zero = tt
suc x > suc y = x > y

--FIXME:This feels like I've got too many lines
_<_ : Nat -> Nat -> Two
zero < zero = ff
zero < _    = tt
_    < zero = ff
suc x < suc y = x < y

_or_ : Two -> Two -> Two
tt or y = tt
ff or y = y
infixl 2 _or_

! : Two -> Two
! ff = tt
! tt = ff


gowhereiam : (Nat /*/ Nat) -> Buffer -> Action
gowhereiam tl b = goRowCol (fst rc) (snd rc) where
                               rc = whereami tl b

gotoline : (Nat /*/ Nat) -> Buffer -> Action
gotoline tl b = goRowCol (fst (whereami tl b)) 0

rcInBuffer : Buffer -> (Nat /*/ Nat)
rcInBuffer (sz <[ cz <[ <> ]> _ ]> _) = blength sz , blength cz

inViewPort : (Nat /*/ Nat) -> (Nat /*/ Nat) -> (Nat /*/ Nat) -> Two
inViewPort (r , c) (w , h) (t , l) = ! (c < l or c > (w + l - 1) or r < t or r > (t + h - 1))

moveViewPort : (Nat /*/ Nat) -> (Nat /*/ Nat) -> (Nat /*/ Nat) -> (Nat /*/ Nat)
moveViewPort (r , c) (w , h) (t , l) = (if r < t then r else (if r > (t + h - 1) then (t + 1) else t)) , (if c < l then c else (if (c > (l + w - 1)) then c else l))


{- EXERCISE 5.3 -}
{- You will need to improve substantially on my implementation of the next component,
   whose purpose is to update the window. Mine displays only one line! -} -- 5/5
--FIXME:WHY?!
{-# NO_TERMINATION_CHECK #-}
render :
  Nat /*/ Nat ->        -- height and width of window -- CORRECTION! width and height
  Nat /*/ Nat ->        -- first visible row, first visible column
  Change /*/ Buffer ->  -- what just happened
  List Action /*/       -- how to update screen
    (Nat /*/ Nat)       -- new first visible row, first visible column
render _ tl (allQuiet , b) =  [] , tl
render wh tl (cursorMove , b) =
                              if inViewPort rc wh tl then (gowhereiam tl b :> []) , tl else render wh tl (bigChange , b)
                              where rc = rcInBuffer b
render wh tl (lineEdit , b) = if inViewPort rc wh tl then (gotoline tl b :> sendText (ensureWidth (w , c) (selectLine b)) :> gowhereiam tl b :> []) , tl else render wh tl (bigChange , b)
                              where rc = rcInBuffer b
                                    w  = fst wh
                                    c  = snd tl
render wh tl (bigChange , b) = drawRegion (selectRegion wh ntl (bufText b)) 0 (snd ntl) (gowhereiam ntl b :> []) , ntl 
                              where rc = rcInBuffer b
                                    ntl = moveViewPort rc wh tl
{- The editor window gives you a resizable rectangular viewport onto the editor buffer.
   You get told
     the current size of the viewport
     which row and col of the buffer are at the top left of the viewport
       (so you can handle documents which are taller or wider than the window)
     the most recent change report and buffer

   You need to figure out whether you need to move the viewport
       (by finding out if the cursor is still within the viewport)
     and if so, where to.

   You need to figure out what to redisplay. If the change report says
     lineEdit and the viewport has not moved, you need only repaint the
     current line. If the viewport has moved or the change report says
     bigChange, you need to repaint the whole buffer.

   You will need to be able to grab a rectangular region of text from the
     buffer, but you do know how big and where from.

   Remember to put the cursor in the right place, relative to where in
     the buffer the viewport is supposed to be. The goRowCol action takes
     *viewport* coordinates, not *buffer* coordinates! You will need to
     invent subtraction!
-}
{- Your code does not need to worry about resizing the window. My code does
   that. On detecting a size change, my code just calls your code with a
   bigChange report and the same buffer, so if you are doing a proper repaint,
   the right thing will happen. -}
{- 2 marks for ensuring that a buffer smaller than the viewport displays
       correctly, with the cursor in the right place, if nobody changes
       the viewport size
   2 marks for ensuring that the cursor remains within the viewport even if
       the viewport needs to move
   1 mark for ensuring that lineEdit changes need only affect one line of
       the display (provided the cursor stays in the viewport)
-}

{- FOR MASOCHISTS ONLY, you have a chance to be even more creative. You have
   spare detectable keys that you could invent meanings for. You also have the
   freedom to change the definition of Buffer, as my code does not care what
   a Buffer is: it only needs to know how to initialize, update and render,
   and these are defined by you.

   Additional structural cursor moves (beginning and end of line, etc) are quite
   easy. Going left or right word-by-word would be more fun: you can match
   against a pattern such as ' '.

   Selection and cut/copy/paste are more challenging. For these, you need to
   modify the Buffer structure to remember the clipboard contents (if any),
   and to manage the extent of any selected region.

   If you feel the need to vary the foreground or background colour of the displayed
   text (e.g. to show a selection), please let me know.

   (SUBTEXT: this exercise is a cut-down version of last year's post-Easter
   task. Feel free to ignore the cutting-down.)
-}


{- Your code then hooks into mine to produce a top level executable! -}
main : IO One
main = mainLoop initBuf (\ k b -> update (keystroke k b)) render

{- To build the editor, just do
     make
   in a shell window (with Ex5 the current directory).
   To run the editor, once compiled, do
     ./Edit
   in the shell window, which should become the editor window.
   To quit the editor, do
     ctrl-C
   like an old-fashioned soul.
-}

{- There is no one right way to do this exercise, and there is some scope for
   extension. It's important that you get in touch if you need help, either in
   achieving the basic deliverable, or in finding ways to explore beyond it.
-}


{- Testy Rubbish -}

hello : List Char
hello = 'h' :> 'e' :> 'l' :> 'l' :> 'o' :> []

my : List Char
my = 'm' :> 'y' :> []

name : List Char
name = 'n' :> 'a' :> 'm' :> 'e' :> []

_sp_ : List Char -> List Char -> List Char
x sp y = x ++ (' ' :> y)
infixl 2 _sp_

lines : List (List Char)
lines = pad 5 [] (hello sp my sp name sp hello sp name)

testbuffer : Buffer
testbuffer = initBuf (lines)

d : Buffer -> Buffer
d (sz <[ cz <[ <> ]> cs ]> []) = sz <: (cz <>> cs) <[ [] <[ <> ]> [] ]> []
d (sz <[ cz <[ <> ]> cs ]> s :> ss) = sz <: (cz <>> cs) <[ [] <[ <> ]> s ]> ss

u : Buffer -> Buffer
u ([] <[ cz <[ <> ]> cs ]> ss) = [] <[ [] <[ <> ]> [] ]> (cz <>> cs) :> ss
u (sz <: s <[ cz <[ <> ]> cs ]> ss) = sz <[ [] <[ <> ]> s ]> (cz <>> cs) :> ss

n : {X : Set} -> (X -> X) -> Nat -> (X -> X)
n f zero = (\ x -> x)
n f (suc nat) = f o (n f nat)

