module Ex6-2-Box where

open import Ex6-Setup
open import Ex6-1-Vec -- -Dummy

---------------------------------------------------------------------------
-- BOXES (5 marks)                                                       --
---------------------------------------------------------------------------

-- Boxes are sized rectangular tilings which fit together precisely.
-- They allow us to talk about the use of 2D space, e.g., on a screen.

data Box (X : Nat -> Nat -> Set)(w h : Nat) : Set where
--        ^basic-tile       width^ ^height

  [_] : X w h -> Box X w h
--      a basic tile is a tiling

  leri : (wl : Nat)   (bl : Box X wl h)
         (wr : Nat)   (br : Box X wr h)
         -> wl + wr == w -> Box X w  h
-- combine "left" and "right" tilings the same height
-- to make a tiling with their total width

  tobo : (ht : Nat)   (bt : Box X w ht)
         (hb : Nat)   (bb : Box X w hb)
         -> ht + hb == h -> Box X w h
-- combine "top" and "bottom" tilings the same width
-- to make a tiling with their total height


---------------------------------------------------------------------------
-- MONADIC STRUCTURE                                                     --
---------------------------------------------------------------------------

-- If X and Y are both kinds of "sized stuff", we can say what it is to be
-- a "size-preserving function" between them.

_[]>_ : (X Y : Nat -> Nat -> Set) -> Set
X []> Y = {w h : Nat} -> X w h -> Y w h
-- A size preserving function turns an X of some size
--                              into a Y the same size.

-- Think of X as "sized placeholders". If we have a way to turn each
-- placeholder into a tiling which fits into the place, we should be
-- able to deploy it across a whole tiling of placeholders. Check
-- that you can achieve that.


_=<<_ : forall {X Y} -> X []> Box Y -> Box X []> Box Y
f =<< [ x ] = f x
f =<< leri wl l wr r x = leri wl (f =<< l) wr (f =<< r) x
f =<< tobo ht t hb b x = tobo ht (f =<< t) hb (f =<< b) x

-- Using _=<<_, rather than more recursion, define... 

mapBox : forall {X Y} -> X []> Y -> Box X []> Box Y
mapBox f = _=<<_ (\x -> [ f x ])

-- roll out a size-preserving function on basic tiles to a whole tiling

joinBox : forall {X} -> Box (Box X) []> Box X
joinBox x = id =<< x
{-
joinBox (leri wl l wr r s) = leri wl (joinBox l) wr (joinBox r) s
joinBox (tobo ht t hb b s) = tobo ht (joinBox t) hb (joinBox b) s
-}
{-
  This looks a lot like the structure of =<< but with the key difference
  that the base case just unpacks to return the box inside. Can't work out
  how to make this a =<< application.
-}


-- turn a tiling-of-tilings into a simple tiling

-- (1 mark) for the lot


---------------------------------------------------------------------------
-- PASTE KITS AND MATRICES                                               --
---------------------------------------------------------------------------

-- A "paste kit" for sized stuff is whatever you need to combine stuff
-- left-to-right and top-to-bottom

record PasteKit (X : Nat -> Nat -> Set) : Set where
  field
    leriPaste : (wl wr : Nat){h : Nat} -> X wl h -> X wr h -> X (wl + wr) h
    toboPaste : {w : Nat}(ht hb : Nat) -> X w ht -> X w hb -> X w (ht + hb)

-- Show that a PasteKit is just what you need to turn a tiling of
-- stuff into some stuff. (1 mark)

pasteBox : {X : Nat -> Nat -> Set} -> PasteKit X -> Box X []> X
pasteBox {X} pk = go where
  open PasteKit pk -- brings leriPaste and toboPaste into scope
  go : Box X []> X
  go [ x ] = x
  go (leri wl l wr r refl) = leriPaste wl wr (go l) (go r)
  go (tobo ht t hb b refl) = toboPaste ht hb (go t) (go b)

-- If you were wondering what any of this had to do with part 1, here we
-- go...

Matrix : Set -> Nat -> Nat -> Set
Matrix C w h = Vec (Vec C w) h
-- matrices are "sized stuff", represented as a vector the right height
-- of rows which are vectors the right width of some sort of unit "cell".

-- Using the equipment you built in part 1, give matrices their PasteKit.
-- (1 mark)

matrixPasteKit : {C : Set} -> PasteKit (Matrix C)
matrixPasteKit = record { leriPaste = λ wl wr l r → (vec _++_) <*> l <*> r
                        ; toboPaste = λ ht hb t b → t ++ b
                        }

---------------------------------------------------------------------------
-- INTERLUDE: TESTING WITH TEXT                                          --
---------------------------------------------------------------------------

-- Turn a list into a vector, either by truncating or padding with
-- a given dummy element.
paddy : {X : Set} -> X -> List X -> {n : Nat} -> Vec X n
paddy _ _         {zero}   = []
paddy x []        {suc n}  = x :: paddy x [] {n}
paddy x (y :: ys) {suc n}  = y :: paddy x ys {n}

-- Use that to make vectors of characters from strings, padding with space.
[-_-] : String -> {n : Nat} -> Vec Char n
[- s -] = paddy ' ' (primStringToList s)

-- Now we can have character matrices of a given size
_*C*_ : Nat -> Nat -> Set
w *C* h = Matrix Char w h

-- Here are some.
mat43-1 : 4 *C* 3
mat43-1 = [- "post" -] :: [- "cake" -] :: [- "flop" -] :: []

mat43-2 : 4 *C* 3
mat43-2 = [- "horn" -] :: [- "walk" -] :: [- "ping" -] :: []

mat22 : 2 *C* 2
mat22 = [- "go" -] :: [- "do" -] :: []

mat62 : 6 *C* 2
mat62 = [- "getter" -] :: [- "gooder" -] :: []

-- Put them together as a tiling.
myTiling : Box _*C*_ 8 5
myTiling = tobo 3 (leri 4 [ mat43-1 ] 4 [ mat43-2 ] refl)
                2 (leri 2 [ mat22 ] 6 [ mat62 ] refl) refl

-- Paste all the pieces and see what you get!
myText : 8 *C* 5
myText = pasteBox matrixPasteKit myTiling


---------------------------------------------------------------------------
-- CUT KITS, MATRICES                                                    --
---------------------------------------------------------------------------

-- A "cut kit" for sized stuff is whatever you need to cut stuff into
-- smaller pieces: left-and-right pieces, or top-and-bottom pieces.

record CutKit (X : Nat -> Nat -> Set) : Set where
  field
    cutLR : (w h wl wr : Nat) -> wl + wr == w ->
            X w h -> X wl h /*/ X wr h
    cutTB : (w h ht hb : Nat) -> ht + hb == h ->
            X w h -> X w ht /*/ X w hb

-- Equip matrices with their CutKit. (1 mark)

matrixTranspose : {C : Set}{w h : Nat} -> Matrix C w h -> Matrix C h w
matrixTranspose [] = vec []
matrixTranspose (h :: hs) with matrixTranspose hs
matrixTranspose (h :: hs) | ws = vec _::_ <*> h <*> ws

matrixCutTB : {C : Set}(w h ht hb : Nat) -> ht + hb == h -> Matrix C w h -> Matrix C w ht /*/ Matrix C w hb
matrixCutTB w .(ht + hb) ht hb refl m = vchop ht m

matrixCutLR : {C : Set}(w h wl wr : Nat) -> wl + wr == w -> Matrix C w h -> Matrix C wl h /*/ Matrix C wr h
matrixCutLR .(wl + wr) h wl wr refl m with matrixCutTB h (wl + wr) wl wr refl (matrixTranspose m)
matrixCutLR .(wl + wr) h wl wr refl m | (t , b) = matrixTranspose t , matrixTranspose b

vmap : {X Y : Set}{n : Nat} -> (X -> Y) -> Vec X n -> Vec Y n
vmap f v = vec f <*> v

noCheese : {C : Set}(w h wl wr : Nat) -> wl + wr == w -> Matrix C w h -> Matrix C wl h /*/ Matrix C wr h
noCheese .(wl + wr) h wl wr refl m = vunzip (vmap (vchop wl) m)

matrixCutKit : {C : Set} -> CutKit (Matrix C)
matrixCutKit {C} = record { cutLR = noCheese --matrixCutLR
                          ; cutTB = matrixCutTB
                          }

---------------------------------------------------------------------------
-- HOLES                                                                 --
---------------------------------------------------------------------------

-- We might want to make sure that, whatever other basic tiles are in play,
-- we can have tiles which are "missing", as if we had cut rectangular
-- holes in a piece of paper.

data HoleOr (X : Nat -> Nat -> Set)(w h : Nat) : Set where
  Hole : HoleOr X w h
  [_] : X w h -> HoleOr X w h

-- A HoleOr X is (you guessed it) either a hole or an X.

-- Show that if X has a CutKit, so has HoleOr X. What do you get when you
-- cut up a hole? (1 mark)

holeOrCutLR : {X : Nat -> Nat -> Set} -> CutKit X -> (w h wl wr : Nat) -> wl + wr == w -> HoleOr X w h -> HoleOr X wl h /*/ HoleOr X wr h
holeOrCutLR ck .(wl + wr) h wl wr refl Hole = Hole , Hole
holeOrCutLR ck .(wl + wr) h wl wr refl [ x ] with CutKit.cutLR ck (wl + wr) h wl wr refl x
holeOrCutLR ck .(wl + wr) h wl wr refl [ x ] | (l , r) = [ l ] , [ r ]

holeOrCutTB : {X : Nat -> Nat -> Set} -> CutKit X -> (w h ht hb : Nat) -> ht + hb == h -> HoleOr X w h -> HoleOr X w ht /*/ HoleOr X w hb
holeOrCutTB ck w .(ht + hb) ht hb refl Hole = Hole , Hole
holeOrCutTB ck w .(ht + hb) ht hb refl [ x ] with CutKit.cutTB ck w (ht + hb) ht hb refl x
holeOrCutTB ck w .(ht + hb) ht hb refl [ x ] | (t , b) = [ t ] , [ b ]

holeCutKit : {X : Nat -> Nat -> Set} -> CutKit X -> CutKit (HoleOr X)
holeCutKit {X} ck = record
                      { cutLR = holeOrCutLR ck
                      ; cutTB = holeOrCutTB ck
                      }


---------------------------------------------------------------------------
-- A BIT OF FUN                                                          --
---------------------------------------------------------------------------

-- Show that you can turn holes into spaces.

holeSpace : HoleOr _*C*_ []> _*C*_
holeSpace Hole = vec (vec ' ')
holeSpace [ x ] = x

-- Show how to render a tiling made of text or holes as text.

renderHoleOrText : Box (HoleOr _*C*_) []> _*C*_
renderHoleOrText = pasteBox matrixPasteKit o mapBox holeSpace

-- Make a test example and see!


myNewTiling : Box (HoleOr _*C*_) (suc (suc (suc (suc (suc (suc (suc zero))))))) (suc (suc (suc (suc zero))))
myNewTiling = leri (suc (suc (suc (suc zero)))) ((tobo (suc (suc (suc zero))) [ [ mat43-1 ] ] (suc zero) [ Hole ] refl))
                   (suc (suc (suc zero))) [ [ matrixTranspose mat43-1 ] ]
              refl

myTest : suc (suc (suc (suc (suc (suc (suc zero)))))) *C* suc (suc (suc (suc zero)))
myTest = renderHoleOrText myNewTiling


---------------------------------------------------------------------------
-- NEXT TIME...                                                          --
---------------------------------------------------------------------------

-- Have a wee think about what you might need to equip Box X with a CutKit.
